package binance.connector;


import java.util.ArrayList;
import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class BinanceMarketDataListener extends MarketDataListener {
	
	
	public BinanceMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://www.binance.com/api/v1/ticker/allBookTickers");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BinanceTicker.class)
			, ""
		);
	}

	
	
	

}
