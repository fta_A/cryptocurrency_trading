package binance.connector;


import javax.net.ssl.HttpsURLConnection;


import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.MarketType;
import connector.OrderStateListener;

public class BinanceOrderStateListener  extends OrderStateListener {

	public BinanceOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls("https://www.binance.com"
				, "/api/v3/order"
				, ""
				, "GET /api/v3/account"
				, ""
				);
		
		this.SetSignMethod("HmacSHA256");
	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		String body = "symbol=" + order.trdPair.toUpperCase()
			+ "&side=" + order.dir.toString().toUpperCase()
			+ "&type=" + order.marketType.toString().toUpperCase()
			//+ "&timeInForce=GTC"
			+ "&quantity=" + String.format("%.12f", order.amount)
			+ (order.marketType == MarketType.limit ? "&price=" + order.price : "")
			+ "&recvWindow=6000000"
			+ "&timestamp=" + nonce;

		return body;
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "recvWindow=6000000"
			+ "&timestamp=" + nonce;

		return body;
	}
	
	@Override
	protected String AdjustBodyAfterBodySign(String path, String body, String signature) {
		return "";
	}

	@Override
	protected String AdjustPathAfterBodySign(String path, String body, String signature) {
		return path + "?" + body + "&signature=" + signature;
	}

	
	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("X-MBX-APIKEY", this.apiKey);
	}
	
	
	@Override
	protected String GetStringToSign(String timestamp, String method, String path, String body) {
		return body;
		//return "symbol=LTCBTC&side=BUY&type=LIMIT&timeInForce=GTC&quantity=1&price=0.1&recvWindow=5000&timestamp=1499827319559";
	}

}
