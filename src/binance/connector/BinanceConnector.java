package binance.connector;

import connector.ExchangeConnector;

public class BinanceConnector extends ExchangeConnector {

	public BinanceConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new BinanceConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new BinanceMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

		this.orderStateListener = new BinanceOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);

	}

}
