package binance.connector;

import interfaces.iTicker;

public class BinanceTicker implements iTicker{
	private String symbol;
	private double bidprice;
	private double bidqty;
	private double askprice;
	private double askqty;


	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getBidprice() {
		return bidprice;
	}

	public void setBidprice(double bidprice) {
		this.bidprice = bidprice;
	}

	public double getBidqty() {
		return bidqty;
	}

	public void setBidqty(double bidqty) {
		this.bidqty = bidqty;
	}

	public double getAskprice() {
		return askprice;
	}

	public void setAskprice(double askprice) {
		this.askprice = askprice;
	}

	public double getAskqty() {
		return askqty;
	}

	public void setAskqty(double askqty) {
		this.askqty = askqty;
	}

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bidprice;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return askprice;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return symbol;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		symbol = id;
	}

}
