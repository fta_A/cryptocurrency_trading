package hitbtc.connector;

import interfaces.iTicker;

public class HitbtcTicker implements iTicker{
	String trdPair;
	
	private double ask;
	private double bid;
	private double last;
	private double low;
	private double high;
	private double open;
	private double volume;
	private double volume_quote;
	private Long timestamp;
	
	

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
		this.ask = ask;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getVolume_quote() {
		return volume_quote;
	}

	public void setVolume_quote(double volume_quote) {
		this.volume_quote = volume_quote;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
