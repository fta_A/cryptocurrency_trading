package hitbtc.connector;


import java.util.HashMap;
import java.util.Map;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class HitbtcMarketDataListener extends MarketDataListener {
	
	
	public HitbtcMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://api.hitbtc.com/api/1/public/ticker");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructMapType(Map.class, String.class, HitbtcTicker.class)
			, ""
		);

	}

	
	
	

}
