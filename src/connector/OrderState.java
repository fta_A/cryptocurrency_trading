package connector;

import interfaces.iOrderState;

public class OrderState implements iOrderState{
	protected String orderNumStr;
	protected Long momentLinuxUTC;
	protected int userReferenceId;
	protected String tradingPair;
	
	
	public void SetOrderNum(String orderNum) {
		this.orderNumStr = orderNum;
	}
	
	public String GetOrderNum() {
		return this.orderNumStr;
	}

	@Override
	public Long MomentLinuxUTC() {
		// TODO Auto-generated method stub
		return this.momentLinuxUTC;
	}

	@Override
	public String GetTrdPair() {
		// TODO Auto-generated method stub
		return this.tradingPair;
	}

	@Override
	public int GetUserRefId() {
		// TODO Auto-generated method stub
		return this.userReferenceId;
	}
	
}
