package connector;


import connector.ConnectionState.StateEnum;
import connector.ConnectionState.TypeEnum;
import interfaces.iConnectionEvent;
import interfaces.iConnectionStateListener;

public class ConnectionStateListener  implements iConnectionStateListener {
	iConnectionEvent iConnectionEvent;	
	FloodControl floodControl;
	int connectionId;
	
	ConnectionState marketDataConnectionState;
	ConnectionState orderStateConnectionState;
	
	public ConnectionStateListener(iConnectionEvent iConnectionEvent, FloodControl floodControl, int connectionId){
		this.iConnectionEvent = iConnectionEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;

		this.marketDataConnectionState = new ConnectionState();
		this.marketDataConnectionState.connectionId = connectionId;
		
		this.orderStateConnectionState = new ConnectionState();
		this.orderStateConnectionState.connectionId = connectionId;
	}
	
	private void InitVariables() {
		this.marketDataConnectionState.connectionType = TypeEnum.MarketData;		
		this.orderStateConnectionState.connectionType = TypeEnum.OrderState;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		this.InitVariables();

		
		this.marketDataConnectionState.connectionState = StateEnum.Connected;
		this.iConnectionEvent.ConnectionStateChanged(marketDataConnectionState);
		
		this.orderStateConnectionState.connectionState = StateEnum.Connected;
		this.iConnectionEvent.ConnectionStateChanged(orderStateConnectionState);

	}

	@Override
	public void MarketDataDisconnected() {
		// TODO Auto-generated method stub
		if(this.marketDataConnectionState.connectionState == StateEnum.Connected) {
			this.marketDataConnectionState.connectionState = StateEnum.Disconnected;
			this.iConnectionEvent.ConnectionStateChanged(marketDataConnectionState);
		}
	}

	@Override
	public void MarketDataConnected() {
		// TODO Auto-generated method stub
		if(this.marketDataConnectionState.connectionState == StateEnum.Disconnected) {
			this.marketDataConnectionState.connectionState = StateEnum.Connected;
			this.iConnectionEvent.ConnectionStateChanged(marketDataConnectionState);
		}
		
	}

	@Override
	public void OrderStateDisconnected() {
		// TODO Auto-generated method stub
		
		if(this.orderStateConnectionState.connectionState == StateEnum.Connected) {
			this.orderStateConnectionState.connectionState = StateEnum.Disconnected;
			this.iConnectionEvent.ConnectionStateChanged(orderStateConnectionState);
		}
		
	}

	@Override
	public void OrderStateConnected() {
		// TODO Auto-generated method stub

		if(this.orderStateConnectionState.connectionState == StateEnum.Disconnected) {
			this.orderStateConnectionState.connectionState = StateEnum.Connected;
			this.iConnectionEvent.ConnectionStateChanged(orderStateConnectionState);
		}
		
	}

	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ClearState() {
		// TODO Auto-generated method stub
		
	}
	
}
