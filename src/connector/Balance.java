package connector;

import interfaces.iBalance;

public class Balance implements iBalance {

	double balanceValue;
	String currencyCode;
	String exchange;
	
	int balance_id;
	double amount;
	
	public Balance() {
		
	}
	
	public Balance(iBalance iBalance) {
		//this.balanceValue = iBalance.BalanceValue();
//		this.amount = iBalance.Availlable();
		this.amount = 0;
		this.balanceValue = 0;
		this.currencyCode = iBalance.CurrencyCode();
		this.exchange = iBalance.Exchange();
		this.balance_id = iBalance.BalanceId();
	}



	public int getBalance_id() {
		return balance_id;
	}

	public void setBalance_id(int balance_id) {
		this.balance_id = balance_id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
		this.balanceValue = amount;
	}

	@Override
	public void setExchange(String exchange) {
		// TODO Auto-generated method stub
		this.exchange = exchange;
	}

	@Override
	public void setCurrencyCode(String currencyCode) {
		// TODO Auto-generated method stub
		this.currencyCode = currencyCode;
	}

	@Override
	public void setBalanceValue(double balanceValue) {
		// TODO Auto-generated method stub
		this.balanceValue = balanceValue;
	}

	
	@Override
	public String Exchange() {
		// TODO Auto-generated method stub
		return this.exchange;
	}

	@Override
	public String CurrencyCode() {
		// TODO Auto-generated method stub
		return this.currencyCode;
	}

	@Override
	public double BalanceValue() {
		// TODO Auto-generated method stub
		return this.balanceValue;
	}

	@Override
	public double Availlable() {
		// TODO Auto-generated method stub
		return this.balanceValue;
	}

	@Override
	public double Pending() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String Address() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "Balance "
				+ "["
				+ "exchnage=" + this.Exchange()
				+ ", currency_code=" + this.CurrencyCode() 
				+ ", amount=" + this.BalanceValue()
				+ ", available=" + this.Availlable() 
				+ "]";
	}

	@Override
	public int BalanceId() {
		// TODO Auto-generated method stub
		return this.balance_id;
	}

}
