package connector;

import interfaces.iTrade;

public class Trade implements iTrade {
	protected String exchange;
	protected String tradeId;
	protected String orderAcceptanceId;
	protected String momentStr;
	protected Long momentLinuxUTC;
	protected String trdPair;
	protected String dir;
	protected double amount;
	protected double price;
	protected double commiss;
	protected int userRefId;
	protected long portfolioTradeId;

	public String toString() {
		return "Trade ["
				+ "exchange=" + exchange
				+ ", userId=" + userRefId
				+ ", trdPair=" + trdPair 
				+ ", tradeid=" + tradeId
				+ ", date=" + momentStr
				+ ", rate=" + price
				+ ", amount=" + amount
				+ ", fee=" + commiss
				+ ", type=" + dir.toLowerCase()
				+ ", accid=" + orderAcceptanceId
				+ "]";
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getExchange()
	 */
	@Override
	public String getExchange() {
		return exchange;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setExchange(java.lang.String)
	 */
	@Override
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getTradeId()
	 */
	@Override
	public String getTradeId() {
		return tradeId;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setTradeId(java.lang.String)
	 */
	@Override
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getOrderAcceptanceId()
	 */
	@Override
	public String getOrderAcceptanceId() {
		return orderAcceptanceId == null ? "" : orderAcceptanceId;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setOrderAcceptanceId(java.lang.String)
	 */
	@Override
	public void setOrderAcceptanceId(String orderAcceptanceId) {
		this.orderAcceptanceId = orderAcceptanceId;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getMomentStr()
	 */
	@Override
	public String getMomentStr() {
		return momentStr;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setMomentStr(java.lang.String)
	 */
	@Override
	public void setMomentStr(String momentStr) {
		this.momentStr = momentStr;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getMomentLinuxUTC()
	 */
	@Override
	public Long getMomentLinuxUTC() {
		return momentLinuxUTC;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setMomentLinuxUTC(java.lang.Long)
	 */
	@Override
	public void setMomentLinuxUTC(Long momentLinuxUTC) {
		this.momentLinuxUTC = momentLinuxUTC;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getTrdPair()
	 */
	@Override
	public String getTrdPair() {
		return trdPair;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setTrdPair(java.lang.String)
	 */
	@Override
	public void setTrdPair(String trdPair) {
		this.trdPair = trdPair;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getDir()
	 */
	@Override
	public String getDir() {
		return dir;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setDir(java.lang.String)
	 */
	@Override
	public void setDir(String dir) {
		this.dir = dir;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getAmount()
	 */
	@Override
	public double getAmount() {
		return amount;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setAmount(double)
	 */
	@Override
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getPrice()
	 */
	@Override
	public double getPrice() {
		return price;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setPrice(double)
	 */
	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getCommiss()
	 */
	@Override
	public double getCommiss() {
		return commiss;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setCommiss(double)
	 */
	@Override
	public void setCommiss(double commiss) {
		this.commiss = commiss;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#getUserRefId()
	 */
	@Override
	public int getUserRefId() {
		return userRefId;
	}

	/* (non-Javadoc)
	 * @see connector.iTrade#setUserRefId(int)
	 */
	@Override
	public void setUserRefId(int userRefId) {
		this.userRefId = userRefId;
	}

	public long getPortfolioTradeId() {
		return portfolioTradeId;
	}

	public void setPortfolioTradeId(long portfolioTradeId) {
		this.portfolioTradeId = portfolioTradeId;
	}

	
}
