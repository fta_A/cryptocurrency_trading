package connector;

import interfaces.iOrderStateListener;
import interfaces.iTrade;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import connector.Order.OrderState;
import interfaces.iAddOrderResult;
import interfaces.iBalance;
import interfaces.iOrderState;
import interfaces.iOrderStateEvent;

public class OrderStateListener  implements iOrderStateListener {
	
	protected iOrderStateEvent iOrderStateEvent;	
	protected FloodControl floodControl;
	protected int connectionId;
	
	protected ArrayList<Order> ordersToSend = new ArrayList<Order>();
	
	protected String apiKey;
	protected String privateKey;
	protected ConnectionStateListener connectionStateListener;
	
	private enum ErrorReason{
		NA
		, InsufficientFunds
		, LostConnection
		, OK
		
	}
	
	protected DecimalFormat decimalFormat;
	
	ErrorReason lastRequestError;
	Long creationMoment = System.currentTimeMillis() / 1000;

	
	HashMap<String, Order> tradesWaitingOrders = new HashMap<>();
	
	public OrderStateListener(iOrderStateEvent iOrderStateEvent, FloodControl floodControl, ConnectionStateListener connectionStateListener){
		this.iOrderStateEvent = iOrderStateEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionStateListener.connectionId;
		this.connectionStateListener = connectionStateListener;
		
		
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
		
		decimalFormat = (DecimalFormat)nf;
		decimalFormat.setMaximumFractionDigits(10);
		decimalFormat.setGroupingUsed(false);
		
		
		//this.RequestClosedOrders("");
	}


	@Override
	public void NewOrder(Order order) {
		// TODO Auto-generated method stub
		
		synchronized(this.ordersToSend) {
			this.ordersToSend.add(order);
		}
	}

	
	
	


	@Override
	public void SetApiKey(String apiKey) {
		// TODO Auto-generated method stub
		this.apiKey = apiKey;
	}


	@Override
	public void SetPrivateKey(String privateKey) {
		// TODO Auto-generated method stub
		this.privateKey = privateKey;
		
	}
	
	HashMap<String, HashMap<Integer, Order>> lostOrders = new HashMap<String, HashMap<Integer, Order>>();
	
	ArrayList<Order> newOrders = new ArrayList<Order>();
	HashMap<Integer, Order> activeOrdersMap = new HashMap<Integer, Order>();
	HashMap<String, Order> killedOrdersMap = new HashMap<String, Order>();
	int requestOrder = 0;
	ArrayList<Order> updatedOrders = new ArrayList<Order>();
	
	protected String URLString;
	protected String addOrderPathString;
	protected String addOrderMethod;
	
	protected String balanceUrl;
	protected String balanceMethod;
	
	protected String tradesUrl;
	protected String tradesMethod;
	
	protected String closedOrdersUrl;
	protected String closedOrdersMethod;
	
	
	boolean bodyInPath = false;
	boolean requestBody = true;
	
	protected ObjectMapper mapper = new ObjectMapper();
	
	String signMethod = "HmacSHA512";
	
	ArrayList<String> balanceNestedNodes = new ArrayList<>();
	JavaType balancesTypeReference;

	
	ArrayList<String> tradesNestedNodes = new ArrayList<>();
	JavaType tradesTypeReference;
	
	ArrayList<String> closedOrdersNestedNodes = new ArrayList<>();
	JavaType ordersTypeReference;
	
	ArrayList<String> addOrderResultNestedNodes = new ArrayList<>();
	JavaType addOrderResultTypeReference;
	
	
	boolean requestedBalance;
	
	String lastTradeId;
	
	ArrayList<String[]> requestedTradesList = new ArrayList<>();
	
	
	Queue<String[]> requestedOrdersList = new LinkedList<>();
	HashSet<String> requestedOrdersPairs = new HashSet<>();
	
	HashMap<String, String> tickersLastTradeMap = new HashMap<String, String>();
	HashMap<String, String> ordersLastMomentMap = new HashMap<String, String>();

	boolean demoTrading = false;
	
	boolean proceed;
	
	
	protected void SetUrls(String mainUrl, String addOrderUrl
			, String killOrderUrl, String balanceUrl, String tradesUrl) {
		// TODO Auto-generated method stub
		this.URLString = mainUrl;
		//this.addOrderPathString = addOrderUrl;
		//this.balanceUrl = balanceUrl;
		
		
		
		
		String[] balanceUrlDetails = balanceUrl.split(" ");
		if(balanceUrlDetails.length == 2) {
			this.balanceMethod  = balanceUrlDetails[0];
			this.balanceUrl = balanceUrlDetails[1];
		}else {
			this.balanceMethod  = balanceUrl;
			this.balanceUrl = "";
		}

		String[] addOrderUrlDetails = addOrderUrl.split(" ");
		if(addOrderUrlDetails.length == 2) {
			this.addOrderMethod  = addOrderUrlDetails[0];
			this.addOrderPathString = addOrderUrlDetails[1];
		}else {
			this.addOrderMethod  = "POST";
			this.addOrderPathString = addOrderUrl;
		}

		String[] tradesUrlDetails = tradesUrl.split(" ");
		if(tradesUrlDetails.length == 2) {
			this.tradesMethod  = tradesUrlDetails[0];
			this.tradesUrl = tradesUrlDetails[1];
		}else {
			this.tradesMethod  = tradesUrl;
			this.tradesUrl = "";
		}


		
	}
	
	
	protected void SetUrls(String mainUrl, String addOrderUrl
			, String killOrderUrl, String balanceUrl, String tradesUrl
			, String closedOrdersUrl) {
		
		this.SetUrls(mainUrl, addOrderUrl, killOrderUrl, balanceUrl, tradesUrl);
		
		
		String[] details = closedOrdersUrl.split(" ");
		if(details.length == 2) {
			this.closedOrdersMethod = details[0];
			this.closedOrdersUrl = details[1];
		}else {
			this.closedOrdersMethod  = closedOrdersUrl;
			this.closedOrdersUrl = "";
		}
	}


	@Override
	public void RequestBallance() {
		// TODO Auto-generated method stub
		this.requestedBalance = true;

		
	}

	@Override
	public void RequestTrades(String trdPair, String from) {
		// TODO Auto-generated method stub
		if(from == null) from = "1";
		synchronized (this.requestedTradesList) {
			this.requestedTradesList.add(new String[] {trdPair, from});
		}
	}
	
	protected void SetBodyInPath() {
		// TODO Auto-generated method stub
		this.bodyInPath = true;
	}
	
	protected void SetSignMethod(String signMethod) {
		// TODO Auto-generated method stub
		this.signMethod = signMethod;
	}
	
	
	
	
	protected void SetTickerAndBalanceTypes(JavaType jsonStructureForTrade, String nestedChainForTrade,
			JavaType jsonStructureForBalance, String nestedChainForBalance) {
		// TODO Auto-generated method stub
		
		this.balancesTypeReference = jsonStructureForBalance;
		if(nestedChainForBalance != "" && nestedChainForBalance != null) {
			String[] levels = nestedChainForBalance.split(":");		
			if(levels.length == 0) this.balanceNestedNodes.add(nestedChainForBalance);
			else 
				for(String level : levels) this.balanceNestedNodes.add(level);
		}

		this.tradesTypeReference = jsonStructureForTrade;
		if(nestedChainForTrade != "" && nestedChainForTrade != null) {
			String[] levels = nestedChainForTrade.split(":");		
			if(levels.length == 0) this.tradesNestedNodes.add(nestedChainForTrade);
			else 
				for(String level : levels) this.tradesNestedNodes.add(level);
		}

	}
	
	
	protected void SetTickerAndBalanceTypes(JavaType jsonStructureForTrade, String nestedChainForTrade
			, JavaType jsonStructureForBalance, String nestedChainForBalance
			, JavaType jsonStructureForOrders, String nestedChainForOrders
			) {
	
		SetTickerAndBalanceTypes(jsonStructureForTrade, nestedChainForTrade, jsonStructureForBalance, nestedChainForBalance);
		
		
		this.ordersTypeReference = jsonStructureForOrders;
		
		if(nestedChainForOrders != "" && nestedChainForOrders != null) {
			String[] levels = nestedChainForOrders.split(":");		
			if(levels.length == 0) this.closedOrdersNestedNodes.add(nestedChainForOrders);
			else 
				for(String level : levels) this.closedOrdersNestedNodes.add(level);
		}

		
	}
	
	protected void SetTickerAndBalanceTypes(JavaType jsonStructureForTrade, String nestedChainForTrade
			, JavaType jsonStructureForBalance, String nestedChainForBalance
			, JavaType jsonStructureForOrders, String nestedChainForOrders
			, JavaType jsonStructureForAddOrderResult, String nestedChainForAddOrderResult
			) {

		SetTickerAndBalanceTypes(jsonStructureForTrade, nestedChainForTrade, jsonStructureForBalance, nestedChainForBalance, jsonStructureForOrders, nestedChainForOrders);
		
		this.addOrderResultTypeReference = jsonStructureForAddOrderResult;	
		
		if(nestedChainForAddOrderResult != "" && nestedChainForAddOrderResult != null) {
			String[] levels = nestedChainForAddOrderResult.split(":");		
			if(levels.length == 0) this.addOrderResultNestedNodes.add(nestedChainForAddOrderResult);
			else 
				for(String level : levels) this.addOrderResultNestedNodes.add(level);
		}
		
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		proceed = true;
		Random rnd = new Random();
		
		//ExecutorService executorService = Executors.newFixedThreadPool(5);

		
		while(proceed){
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			ArrayList<Order> tmpArrayList_Order;
			synchronized(this.ordersToSend) {
				if(this.ordersToSend.size() > 0){
					tmpArrayList_Order = this.ordersToSend;
					this.ordersToSend = this.newOrders;
					this.newOrders = tmpArrayList_Order;
				}
			}
			
			for(Order order: this.newOrders) {
				switch(order.orderType) {
				case NewOrder:
					//executorService.execute(new Runnable() {
					//    public void run() {
					        AddOrder(order);
					//    }
					//});
					
				break;
				case KillOrder:
					Order activeOrder = this.activeOrdersMap.get(order.userref);
					if(activeOrder != null) KillOrder(activeOrder);
				break;
				default:
					break;
				}
			}			
			this.newOrders.clear();
			
			if(this.activeOrdersMap.size() > 0 
					|| this.requestedOrdersList.size() > 0
					|| this.requestedBalance
					|| this.requestedTradesList.size() > 0			
					) {
				synchronized (this.floodControl) {
					int availableRequests = this.floodControl.AvailableRequests();
					
					if(availableRequests > 0){
						while(true) {
							if(requestOrder == 0) {
								if(this.activeOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 4;
								else break;
							}
							
							if(requestOrder == 1) {
								if(this.requestedOrdersList.size() == 0) requestOrder = (requestOrder + 1) % 4;
								else break;
							}
							
							if(requestOrder == 2) {
								if(!this.requestedBalance) requestOrder = (requestOrder + 1) % 4;
								else break;
							}

							if(requestOrder == 3) {
								if(this.requestedTradesList.size() == 0) requestOrder = (requestOrder + 1) % 4;
								else break;
							}
						}
						
						/*
						executorService.execute(new Runnable() {
						    public void run() {
						    	System.out.println("THR:" + requestOrder);
						  */  	
								switch(requestOrder) {
								case 0:
									CheckOpenedOrders();
									break;
								case 1:
									CheckClosedOrders();
									break;
								case 2:									
									GetRequestedBallance();
									break;
								case 3:
									GetRequestedTrades();
									break;
								}			
								/*
						    }
						});
						*/
						
						
						requestOrder = (requestOrder + 1) % 2;					
						this.floodControl.NewRequest();
					}
				}
			}
			
			
			for(Order order : this.updatedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.updatedOrders.clear();
		}
		
	}

	private void KillOrder(Order order) {
		
	}
	
	private void RequestClosedOrders(String trdPair) {
		synchronized (this.requestedOrdersList) {
			String from = ordersLastMomentMap.get(trdPair);
			if(from == null) from =  String.valueOf(creationMoment.toString());
			
			if(this.requestedOrdersPairs.add(trdPair)) {				
				this.requestedOrdersList.add(new String[] {trdPair, from});
			}
		}
	}
	
	private void CheckClosedOrders() {
		if(this.requestedOrdersList.size() == 0) return;
		
		
		String[] ordDetails = this.requestedOrdersList.remove();
		//this.requestedOrdersPairs.remove(ordDetails[0]);
		
		if(this.ordersTypeReference == null) {
			System.out.println(Thread.currentThread().getName() + ".CheckClosedOrders() NOT IMPLEMENTED");
			
			//HandleLostOrders(ordDetails);
			return;
		}
		
		String timestamp = String.valueOf(System.currentTimeMillis());
		String body = GetClosedOrdersBody(timestamp, ordDetails[0], ordDetails[1]);
		String path = this.closedOrdersUrl;
		String method = this.closedOrdersMethod;
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp, closedOrdersNestedNodes);
		if(jn == null)
			if(lastRequestError == ErrorReason.OK) {
				HandleLostOrders(ordDetails);
				return;
			}else {
				this.requestedOrdersList.add(ordDetails);
				return;
			}

		this.connectionStateListener.OrderStateConnected();

		Long lastTrdMoment = (long) 0;
		String fromParam = "";
		
		
		boolean allOrders = false;
		ArrayList<iOrderState> list = GetOrdersList(jn, this.ordersTypeReference);
		
		String trdPair = list.size() == 0 ? "" : list.get(0).GetTrdPair();
		for(iOrderState orderState : list) {
			if(!orderState.GetTrdPair().equals(trdPair)) {
				allOrders = true;
				trdPair = orderState.GetTrdPair();
			}
			 
			if(this.lostOrders.containsKey(trdPair)) {
				Order order = this.lostOrders.get(trdPair).remove(orderState.GetUserRefId());
				if(order != null) {
		    		order.orderState = OrderState.Executed;
		    		order.initAmount = order.amount;
		    		order.amount = 0;		
		    		order.orderNum = orderState.GetOrderNum();
		    		this.updatedOrders.add(order);
		    		
		    		
		    		this.tradesWaitingOrders.put(order.orderNum, order);
				}
				
				
			}
				
			
			if(orderState.MomentLinuxUTC() >= lastTrdMoment) {
				lastTrdMoment = orderState.MomentLinuxUTC();
				fromParam = GetFromForClosedOrdersRequest(orderState);
			}
		}
		
		
		
		if(lastTrdMoment != 0) {
			this.ordersLastMomentMap.put(ordDetails[0], fromParam);
			this.RequestTrades(ordDetails[0], this.tickersLastTradeMap.get(ordDetails[0]));
		}
		
		if(allOrders) {
			
			while(ordDetails != null) {
				HandleLostOrders(ordDetails);
				ordDetails = this.requestedOrdersList.poll();
			}
			
			
			
		}else {
			HandleLostOrders(ordDetails);
		}
		
		
	
	}
	
	private void HandleLostOrders(String[] ordDetails) {
		HashMap<Integer, Order> ordersByTrdPair = this.lostOrders.get(ordDetails[0]);
		if(ordersByTrdPair == null || ordersByTrdPair.size() == 0) {
			
			this.requestedOrdersPairs.remove(ordDetails[0]);
			return;
		}
		
		Iterator<Entry<Integer, Order>> jt = ordersByTrdPair.entrySet().iterator();
        while (jt.hasNext()) {
        	Entry<Integer, Order> orderPair = jt.next();
        	
        	Order order = orderPair.getValue();
        	
        	if(System.currentTimeMillis() - order.sentUnixTime >= 300000) {
        		NewOrder(order);	
        		ordersByTrdPair.remove(orderPair.getKey());
        	}
        		
        	//jt.remove();
        }
        
        
        if(ordersByTrdPair.size() == 0) {
			this.requestedOrdersPairs.remove(ordDetails[0]);
        }else {
        	this.requestedOrdersList.add(ordDetails);
        	//this.requestedOrdersPairs.add(ordDetails[0]);
        }
        
	}
	
	private ArrayList<iOrderState> GetOrdersList(JsonNode jn, JavaType typeReference) {
		ArrayList<iOrderState> result = null;
		
		if(typeReference.isMapLikeType()) {
			Map<String, iOrderState> marketMap = mapper.convertValue(jn, typeReference);
			if(marketMap == null) return null;
			
			result = new ArrayList<>();
			for(Map.Entry<String, iOrderState> entry : marketMap.entrySet()) {
				iOrderState value;
				
				if(entry.getValue() instanceof iOrderState)
					value = entry.getValue();
				else {
					value = new connector.OrderState();					
				}
				
				value.SetOrderNum(entry.getKey());
				
				result.add(value);
			}
			
		}else if (typeReference.isCollectionLikeType()){
			result = mapper.convertValue(jn, typeReference);
		}else {
			
		}
		
		return result;
	}
	
	
	
	private void AddOrder(Order order) {
	 	String timestamp = String.valueOf(System.currentTimeMillis());
		String body = GetAddOrderBody(order, timestamp);		
		if(body == null)return;
		
		String path = this.addOrderPathString;
		String method = this.addOrderMethod;
		
		order.sentUnixTime = System.currentTimeMillis();
		
		JsonNode jn;
		if(!this.demoTrading)
			jn = getPrivateAnswer(path, body, method, timestamp, this.addOrderResultNestedNodes);
		else
			jn = mapper.createObjectNode();
		
		//System.out.println("ADD{" + this.connectionId + "}: " + order.toString() + ". Answer: " + jn);
		
		if(jn != null) {
			this.connectionStateListener.OrderStateConnected();
			iAddOrderResult addOrderResult = mapper.convertValue(jn, this.addOrderResultTypeReference);
			
			switch(order.marketType) {
			case market:{
	    		order.orderState = OrderState.Executed;
	    		order.initAmount = order.amount;
	    		order.amount = 0;
	    		
	    		this.updatedOrders.add(order);				
			}
			break;
			case limit:{
				
			}break;
			}
	        
			order.orderNum = addOrderResult.getOrderNumber();
			
	    	this.RequestBallance();	    	
	    	
	    	ArrayList<iTrade> orderTrades = addOrderResult.getOrderTrades();
	    	if(orderTrades != null)
				for(iTrade trade : orderTrades) {
					trade.setUserRefId(order.userref);
					trade.setPortfolioTradeId(order.portfolioTradeId);
					trade.setOrderAcceptanceId(addOrderResult.getOrderNumber());
					trade.setTrdPair(order.trdPair);
					
					
					this.iOrderStateEvent.TradeExecuted(trade);
				}
			else {
				this.tradesWaitingOrders.put(order.orderNum, order);				
				this.RequestTrades(order.trdPair, this.tickersLastTradeMap.get(order.trdPair));
			}
	    	
		}else {
			switch(this.lastRequestError) {
			case InsufficientFunds:{
				order.orderState = OrderState.Refused;
				this.updatedOrders.add(order);					
			}break;
			case LostConnection:{
				if(!this.lostOrders.containsKey(order.trdPair)) {
					HashMap<Integer, Order> hm = new HashMap<>();
					this.lostOrders.put(order.trdPair, hm);
				}
				this.lostOrders.get(order.trdPair).put(order.userref, order);
				
				RequestClosedOrders(order.trdPair);
					
			}break;
			default:{
				this.connectionStateListener.OrderStateDisconnected();
				NewOrder(order);
			}break;
				
			}
			
		}
		
	}
	
	
	private JsonNode getPrivateAnswer(String path, String body, String method, String timestamp, ArrayList<String> nestedNodes) {
		JsonNode jsonReply = null;
		lastRequestError = ErrorReason.NA;
		
		if(privateKey == null) return null;
		
		String sign = calculateSignature(timestamp, method, path, body);
		
		path = AdjustPathAfterBodySign(path, body, sign);
		body = AdjustBodyAfterBodySign(path, body, sign);
		
		HttpsURLConnection httpsURLConnection = null;
		
		try {
			URL postURL;
			if(this.bodyInPath)
				postURL	= new URL(this.URLString + path + body);
			else
				postURL	= new URL(this.URLString + path);
			
			httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
			httpsURLConnection.setRequestMethod(method);
			httpsURLConnection.setConnectTimeout(10000);
			
			this.SetRequestProperties(httpsURLConnection, sign, timestamp, body);
			
			httpsURLConnection.setDoOutput(true);
			
			
			if(this.requestBody && method.equals("POST")) {
				OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
		        writer.write(body);
		        writer.flush();
			}
			
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine().toLowerCase();
	      
	        
	        jsonReply = mapper.readTree(jsonString);
	        
	        if(nestedNodes != null)
		        for(String nest : nestedNodes) {
		        	jsonReply = jsonReply.get(nest);
		        	
		        	if(jsonReply == null) {
		        		
		        		jsonString = jsonString.replaceAll("[{}\\[\\]\"]", " ");
		        		
		        		String jsonError = "{"
		        				+ "\"connection_id\":" + this.connectionId
		        				+ ", \"path\":\"" + path + "\""
		        				+ ", \"body\":\"" + body + "\""
		        				+ ", \"method\":\"" + method + "\""
		        				+ ", \"timestamp\":\"" + timestamp + "\""
		        				+ ", \"description\":\"Wrong nested nodes structure.\""
		        				+ ", \"misc\":\"" + jsonString + "\""
        						+ "}";
		        		
		        		this.iOrderStateEvent.ExceptionIsAraised(jsonError);
		        		
		        		return null;
		        	}
		        }
	        
	        
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			if(httpsURLConnection == null) {
				lastRequestError = ErrorReason.LostConnection;
				
				
				
				String jsonError = "{"
        				+ "\"connection_id\":" + this.connectionId
        				+ ", \"path\":\"" + path + "\""
        				+ ", \"body\":\"" + body + "\""
        				+ ", \"method\":\"" + method + "\""
        				+ ", \"timestamp\":\"" + timestamp + "\""
        				+ ", \"description\":\"httpsURLConnection is null.\""
        				+ ", \"misc\":\"" + "\""
						+ "}";
        		
        		this.iOrderStateEvent.ExceptionIsAraised(jsonError);
				
				return null;
			}
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getErrorStream()));
	        String errorString = null;
			try {
				errorString = httpsURLConnection.getResponseCode() + ". " + httpsURLConnection.getResponseMessage() + ":" + bufferedReader.readLine();
				
				errorString = errorString.replaceAll("[{}\\[\\]\"]", " ");
				
				String jsonError = "{"
        				+ "\"connection_id\":" + this.connectionId
        				+ ", \"path\":\"" + path + "\""
        				+ ", \"body\":\"" + body + "\""
        				+ ", \"method\":\"" + method + "\""
        				+ ", \"timestamp\":\"" + timestamp + "\""
        				+ ", \"description\":\"GetInputStream() exception\""
        				+ ", \"misc\":\"" + errorString + "\""
						+ "}";
        		
        		this.iOrderStateEvent.ExceptionIsAraised(jsonError);
				
				
				
				if(httpsURLConnection.getResponseCode() == 502) {
					jsonReply = mapper.readTree("{ "
							+ "\"text\": \"Maybe there is a trade.\", "
							+ "\"error\": \"502\""
							+ "}");
					

					lastRequestError = ErrorReason.LostConnection;					
					return null;
				}
				
				lastRequestError = ErrorReason.LostConnection;
				return null;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.out.println(errorString);
			
			if(errorString.contains("Insufficient funds"))
				lastRequestError = ErrorReason.InsufficientFunds;
			else
				lastRequestError = ErrorReason.NA;
			
			return null;
		}
		
		lastRequestError = ErrorReason.OK;
		return jsonReply;
	}
	
	
	//Virtual Methods
	
	protected String AdjustBodyAfterBodySign(String path, String body, String signature) {
		return body;
	}
	protected String AdjustPathAfterBodySign(String path, String body, String signature) {
		return path;
	}
	protected String GetStringToSign(String nonce, String method, String path, String body) {
		return body;
	}
	protected String GetAddOrderBody(Order order, String nonce) {
		return null;
	}
	protected String GetAddOrderPath(Order order, String nonce) {
		return null;
	}
	protected String GetBalanceBody(String nonce) {
		return "";
	}
	protected String GetClosedOrdersBody(String nonce, String trdPair, String from) {
		return "";
	}
	
	
	protected String GetTradesBody(String nonce, String trdPair, String from) {
		return "";
	}
	protected String GetTradesPath(String path, String nonce, String trdPair, String from) {
		return path;
	}
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		
	}
	protected String calculateSignature(String nonce, String method, String path, String body) {
		String signature = "";
	    try {
	    	String text = GetStringToSign(nonce, method, path, body);
	    	
	    	Mac mac = Mac.getInstance(this.signMethod);
	        SecretKeySpec secret_key = new SecretKeySpec(privateKey.getBytes(), this.signMethod);
	        mac.init(secret_key);
	        signature = Hex.encodeHexString(mac.doFinal(text.getBytes()));
	    } catch(Exception e) {}
	    return signature;			
	}	
	protected String GetFromForGetTradeRequest(iTrade trade) {
		return "1";
	}
	
	protected String GetFromForClosedOrdersRequest(iOrderState orderState) {
		return "1";
	}
	
	
	//Private Methods
	
	private void CheckOpenedOrders() {
		
	}
	

	
	private void GetRequestedBallance() {
		this.requestedBalance = false; 
		
		String timestamp = String.valueOf(System.currentTimeMillis());
		String body = GetBalanceBody(timestamp);
		String path = this.balanceUrl;
		String method = this.balanceMethod;
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp, this.balanceNestedNodes);
		if(jn == null) return;
		
		ArrayList<iBalance> balancesList = GetBalancesList(jn, this.balancesTypeReference);
		for(iBalance balance : balancesList) {
			this.iOrderStateEvent.BalanceStateChanged(balance);
//			System.out.println(balance.toString());
		}
	}
	
	private ArrayList<iBalance> GetBalancesList(JsonNode jn, JavaType typeReference) {
		ArrayList<iBalance> result = null;
		
		if(typeReference.isMapLikeType()) {
			Map<String, iBalance> marketMap = mapper.convertValue(jn, typeReference);
			if(marketMap == null) return null;
			
			result = new ArrayList<>();
			for(Map.Entry<String, iBalance> entry : marketMap.entrySet()) {
				iBalance value;
				
				if(entry.getValue() instanceof iBalance)
					value = entry.getValue();
				else {
					value = new Balance();
					value.setBalanceValue(mapper.convertValue(entry.getValue(), double.class));
				}
				
				value.setCurrencyCode(entry.getKey());	
				
				result.add(value);
			}
			
		}else if (typeReference.isCollectionLikeType()){
			result = mapper.convertValue(jn, typeReference);
		}else {
			
		}
		
		return result;
	}
	
	private void GetRequestedTrades() {
		String[] trdDetails = this.requestedTradesList.remove(this.requestedTradesList.size()-1);
		
		String timestamp = String.valueOf(System.currentTimeMillis());
		String body = GetTradesBody(timestamp, trdDetails[0], trdDetails[1]);
		String path = GetTradesPath(this.tradesUrl, "", trdDetails[0], trdDetails[1]);
		String method = this.tradesMethod;
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp, this.tradesNestedNodes);
		if(jn == null) return;
		
		
		Long lastTrdMoment =(long) 0;
		String fromParam = "";
		
		ArrayList<iTrade> tradesList = GetTradesList(jn, this.tradesTypeReference);
		for(iTrade trade : tradesList) {
			if(trade.getTrdPair() == null)	trade.setTrdPair(trdDetails[0]);
			
			Order order = this.tradesWaitingOrders.get(trade.getOrderAcceptanceId());
			if(order != null) {
				trade.setUserRefId(order.userref);
				trade.setPortfolioTradeId(order.portfolioTradeId);
				trade.setTrdPair(order.trdPair);
				
				order.initAmount -= trade.getAmount();
				if(order.amount == 0)this.tradesWaitingOrders.remove(trade.getOrderAcceptanceId());
			}
			
			this.iOrderStateEvent.TradeExecuted(trade);
			
		//	System.out.println(trade.toString());
			
			if(trade.getMomentLinuxUTC() > lastTrdMoment) {
				lastTrdMoment = trade.getMomentLinuxUTC();
				fromParam = GetFromForGetTradeRequest(trade);
			}
		}
		
		if(lastTrdMoment != 0) this.tickersLastTradeMap.put(trdDetails[0], fromParam);
		//this.RequestTrades(trdDetails[0], fromParam);

		//System.out.println("KKKK");
	}

	
	private ArrayList<iTrade> GetTradesList(JsonNode jn, JavaType typeReference) {
		ArrayList<iTrade> result = null;
		
		if(typeReference.isMapLikeType()) {
			Map<String, iTrade> marketMap = mapper.convertValue(jn, typeReference);
			if(marketMap == null) return null;
			
			result = new ArrayList<>();
			for(Map.Entry<String, iTrade> entry : marketMap.entrySet()) {
				iTrade value;
				
				if(entry.getValue() instanceof iTrade) {
					value = entry.getValue();
					value.setTradeId(entry.getKey());
				}else {
//					value = new Trade();
//					value.setBalanceValue(mapper.convertValue(entry.getValue(), double.class));
					
					System.out.println("Method not implemented.");
					return null;
				}
				
//				value.setTrdPair(entry.getKey());	
				
				result.add(value);
			}
			
		}else if (typeReference.isCollectionLikeType()){
			result = mapper.convertValue(jn, typeReference);
		}else {
			
		}
		
		return result;
	}
	
	protected void DisableWriteToBody() {
		this.requestBody = false;
	}


	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub
		
		proceed = false;
	}


	@Override
	public void ClearState() {
		// TODO Auto-generated method stub
		
	}
	

}
