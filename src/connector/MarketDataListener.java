package connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import interfaces.iMarketDataListener;
import interfaces.iTicker;
import interfaces.iMarketDataEvent;

public class MarketDataListener implements iMarketDataListener{

	protected iMarketDataEvent iMarketDataEvent;	
	protected FloodControl floodControl;
	protected int connectionId;
	protected HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	protected HashSet<String> subscribedHashSet = new HashSet<String>();
	protected ConnectionStateListener connectionStateListener;
	
	protected boolean fullSubscription;
	

	private JavaType typeReference;
	private ArrayList<String> nestedNodes;
	
	protected ObjectMapper mapper;
	
	
	private ArrayList<String> tickersList;
	private ArrayList<URL> urlList;
	private int tickersURLListPosition = 0;

	private URL tickersListURL;
	private long lastTickersListRequestMoment = 0;
	private JavaType typeReferenceForTickersList;
	private ArrayList<String> nestedNodesForTickersList;

	
	private TickersListType tickersListType;
	private String separator;

	public enum TickersListType{
		ManyToOne,
		OneToOne
	}

	String urlForTickersList;
	String urlForTicker;
	String urlForTickerEnd;
	
	boolean useHttpsProtocol;
	boolean browserEmulation;

	boolean proceed;
	
	
	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, ConnectionStateListener connectionStateListener){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionStateListener.connectionId;
		this.connectionStateListener = connectionStateListener;
		
		this.fullSubscription = false;
		
		this.mapper = new ObjectMapper();		
		this.nestedNodes = new ArrayList<>();
		
		this.tickersList = new ArrayList<>();
		this.urlList = new ArrayList<>();
		
		this.tickersListURL = null;
		this.nestedNodesForTickersList = new ArrayList<>();
		
		this.useHttpsProtocol = true;
		this.browserEmulation = false;
		
		
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	}
	
	
	public void SetTickersListSource(TickersListType listType, String separator, String urlForTickersList, JavaType jsonStructureForTickersList, String nestedChainForTickersList,  String urlForTicker, String urlForTickerEnd, JavaType jsonStructureForTicker, String nestedChainForTicker) {
		this.tickersListType = listType;
		this.urlForTickersList = urlForTickersList;
		this.urlForTicker = urlForTicker;
		this.urlForTickerEnd = urlForTickerEnd;
		this.typeReference = jsonStructureForTicker;
		this.typeReferenceForTickersList = jsonStructureForTickersList;
		this.separator = separator;
		
		if(nestedChainForTicker != "" && nestedChainForTicker != null) {
			String[] levels = nestedChainForTicker.split(":");		
			if(levels.length == 0) this.nestedNodes.add(nestedChainForTicker);
			else 
				for(String level : levels) this.nestedNodes.add(level);
		}

		if(nestedChainForTickersList != "" && nestedChainForTickersList != null) {
			String[] levels = nestedChainForTickersList.split(":");	
			if(levels.length == 0) this.nestedNodesForTickersList.add(nestedChainForTickersList);
			else
				for(String level : levels) this.nestedNodesForTickersList.add(level);
		}

	}
	
	public void SetBrowserEmulation() {
		this.browserEmulation = true;
	}

	public void SetTickersListSource(HashMap<String, String> urlsMap,  JavaType jsonStructureForTicker, String nestedChainForTicker) {
		this.urlList.clear();
		this.tickersList.clear();
		this.tickersURLListPosition = 0;

		for(Map.Entry<String, String> entry : urlsMap.entrySet()) {
			try {
				this.urlList.add(new URL(entry.getValue()));
				this.tickersList.add(entry.getKey());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.urlList.size() > 0 && this.urlList.get(0).getProtocol().equals("http"))
			this.useHttpsProtocol = false;
		
		this.typeReference = jsonStructureForTicker;
		
		if(nestedChainForTicker != "" && nestedChainForTicker != null) {
			String[] levels = nestedChainForTicker.split(":");		
			if(levels.length == 0) this.nestedNodes.add(nestedChainForTicker);
			else 
				for(String level : levels) this.nestedNodes.add(level);
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		proceed = true;
		Random rnd = new Random();

		while(proceed){
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.FillAvailableTickersList();
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					this.floodControl.NewRequest();
					
					if(!this.fullSubscription && this.tickersList.size() > 0) {
						String tickerForRequest = this.tickersList.get(tickersURLListPosition);
						
						while (tickerForRequest != "" && !this.subscribedHashSet.contains(tickerForRequest)) {
							tickersURLListPosition = (tickersURLListPosition + 1) % tickersList.size();
							tickerForRequest = this.tickersList.get(tickersURLListPosition);
						}
					}
					
					URL marketDataUrl = urlList.get(tickersURLListPosition);
					
					JsonNode jn = GetJsonNodeFromURL(marketDataUrl, this.nestedNodes);
					
					ArrayList<iTicker> marketDataList = GetMarketDataList(jn, this.typeReference);//mapper.convertValue(jn, typeReference);
					
					if(marketDataList != null) {
						this.connectionStateListener.MarketDataConnected();
						
						for(iTicker ticker : marketDataList) {
							String tickerId = ticker.Id();
							if(tickerId == null) tickerId = this.tickersList.get(tickersURLListPosition);
							
							
							
							boolean subscribed = false;							
							synchronized (this.subscribedHashSet) {
								subscribed = this.subscribedHashSet.contains(tickerId);
							}							
							
							if(subscribed || this.fullSubscription) {
								MarketData marketData = marketDataHashMap.get(tickerId);
								if(marketData == null) {
									marketData = new MarketData();
									marketData.setTrdPair(tickerId);
									marketDataHashMap.put(tickerId, marketData);
								}
								
								if(marketData.getHighestBid() != ticker.Bid() || marketData.getLowestAsk() != ticker.Ask()) {
									marketData.setHighestBid(ticker.Bid());
									marketData.setLowestAsk(ticker.Ask());
		
									this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
								}
							}
						}
					}else
						this.connectionStateListener.MarketDataDisconnected();

					
					tickersURLListPosition = (tickersURLListPosition + 1) % tickersList.size();
				}
			}
		}

	}
	
	private void FillAvailableTickersList() {
		if(this.urlForTickersList == null) return;
		
		//86,400,000 = 24 * 60 * 60 * 1000 = 1 day
		if((System.currentTimeMillis() - this.lastTickersListRequestMoment < 86400000)
				 && (this.tickersList.size() != 0)
				) return;

		
		
		try {
			this.tickersListURL = new URL(this.urlForTickersList);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		synchronized (this.floodControl) {
			int availableRequests = this.floodControl.AvailableRequests();
			
			if(availableRequests > 0){
				this.floodControl.NewRequest();
				
				this.urlList.clear();
				this.tickersList.clear();
				
				
				JsonNode jn = GetJsonNodeFromURL(this.tickersListURL, this.nestedNodesForTickersList);

				ArrayList<iTicker> marketDataList = GetMarketDataList(jn, this.typeReferenceForTickersList);
				if(marketDataList != null) {
					String tradingPairs = "";
					for(iTicker ticker : marketDataList) {
						switch(this.tickersListType) {
						case ManyToOne:
							tradingPairs += this.separator + ticker.Id();
							break;
						case OneToOne:
							String tickerURLString = this.urlForTicker + ticker.Id() + this.urlForTickerEnd;
							try {
								this.urlList.add(new URL(tickerURLString));
								this.tickersList.add(ticker.Id());
							} catch (MalformedURLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							break;
						}
						
						
					}
					
					if(this.tickersListType == TickersListType.ManyToOne) {
						try {
							URL currentURL = new URL(this.urlForTicker + tradingPairs.substring(1));
							this.urlList.add(currentURL);
							this.tickersList.add("");
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
				
				this.tickersURLListPosition = 0;
				this.lastTickersListRequestMoment = System.currentTimeMillis();
			}
		}
		
	}
	
	
	
	protected ArrayList<iTicker> GetMarketDataList(JsonNode jn, JavaType typeReference){
		if(jn == null) return null;
		ArrayList<iTicker> marketDataList = null;
		
		if(typeReference.isMapLikeType()) {
			Map<String, iTicker> marketMap = mapper.convertValue(jn, typeReference);
			if(marketMap == null) return null;
			
			marketDataList = new ArrayList<>();
			for(Map.Entry<String, iTicker> entry : marketMap.entrySet()) {
				iTicker ticker = entry.getValue();
				ticker.setId(entry.getKey());	
				
				marketDataList.add(ticker);
			}
		}else if (typeReference.isCollectionLikeType()){
			marketDataList = mapper.convertValue(jn, typeReference);
			
			if(marketDataList != null && marketDataList.size() > 0)
				if(!(marketDataList.get(0) instanceof iTicker)){
					ArrayList<String> trdPairsList = mapper.convertValue(jn, typeReference);
					marketDataList = new ArrayList<>();
					for(String trdPair : trdPairsList) {
						Ticker ticker = new Ticker();
						
						ticker.setId(trdPair);						
						marketDataList.add(ticker);
					}
				}
				
			
			
		}else {
			iTicker ticker = mapper.convertValue(jn, typeReference);
			if(ticker == null) return null;
			
			marketDataList = new ArrayList<>();
			marketDataList.add(ticker);
		}
		
		return marketDataList;
	}

	@Override
	public void Subscribe(String tickerCode) {
		// TODO Auto-generated method stub
		synchronized (this.subscribedHashSet) {			
			this.subscribedHashSet.add(tickerCode);	
		}
	}

	@Override
	public void SubscribeAll() {
		// TODO Auto-generated method stub
		this.fullSubscription = true;
	}
	
	private JsonNode GetJsonNodeFromURL(URL url, ArrayList<String> nestedNodes) {
		if (url == null) return null;
 		
		HttpURLConnection urlConnection = null;
		
		int responseCode;
		try {
			if(this.useHttpsProtocol) urlConnection = (HttpsURLConnection)url.openConnection();
			else urlConnection = (HttpURLConnection)url.openConnection();
			
			urlConnection.setConnectTimeout(10000);
			if(this.browserEmulation) urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

			
			responseCode = urlConnection.getResponseCode();
		} catch (IOException e) {
			this.floodControl.FreezeAndAdjustFloodPeriod(5000, 0);
			return null;
		}
			
		String responseString;
		
		try {
			InputStream inputStream = null;
			
			if(responseCode < HttpURLConnection.HTTP_BAD_REQUEST)
				inputStream = urlConnection.getInputStream();
			else
				inputStream = urlConnection.getErrorStream();

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			responseString = bufferedReader.readLine();
			
		} catch (IOException e) {
			this.floodControl.FreezeAndAdjustFloodPeriod(5000, 0);
			return null;			
		}
			
			
			
		switch(responseCode) {
		case HttpURLConnection.HTTP_OK:{
			JsonNode jn  = null;

			if(responseString == null) return null;
			
			try {
				jn = mapper.readTree(responseString.toLowerCase());
			} catch (IOException e) {
				return null;
			}	
			
			for(String node : nestedNodes) {
				if(jn == null) return null;
				jn = jn.get(node);
			}
			
			return jn;
		}
		case 429:
			this.floodControl.FreezeAndAdjustFloodPeriod(10000, 3);
			return null;
		case 400:
			return null;
		default:
			//System.out.println(responseString);
			return null;
		}
		
	}

	void HandleURLException(int responseCode, String streamMessage) {
		switch(responseCode) {
		case 429:
			this.floodControl.FreezeAndAdjustFloodPeriod(10000, 3);
			break;
		case 400:
			break;
		default:
			System.out.println(streamMessage);
		}
	}

	

	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub
		
		ClearState();
		this.proceed = false;
	}


	@Override
	public void ClearState() {
		// TODO Auto-generated method stub
		
		synchronized (this.subscribedHashSet) {			
			this.subscribedHashSet.clear();
		}
		
		this.fullSubscription = false;

	}

}
