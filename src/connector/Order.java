package connector;

import java.util.ArrayList;

import interfaces.iExchangeConnector;

public class Order {
	public long portfolioTradeId;
	public int userref;
	public String orderNum;
	public String trdPair;
	public OrderDirection dir;
	public MarketType marketType;
	public double price;
	public double amount;
	public double initAmount;
	public OrderState orderState;
	public OrderType orderType;
	public String errorState;
	public long sentUnixTime;
	
	public ArrayList<String> transIdList = new ArrayList<String>();
	public iExchangeConnector exchangeConnector;
	public Object parent;
	
	public Order getCopy() {
		Order order = new Order();
		order.portfolioTradeId = this.portfolioTradeId;
		order.orderNum = this.orderNum;
		order.amount = this.amount;
		order.dir = this.dir;
		order.marketType = this.marketType;
		order.orderState = this.orderState;
		order.price = this.price;
		order.trdPair = this.trdPair;
		order.userref = this.userref;
		order.exchangeConnector = this.exchangeConnector;
		order.parent = this.parent;
		order.sentUnixTime = this.sentUnixTime;
		
		
		
		return order;
	}
	
	
	
	@Override
	public String toString() {
		return "Order [userref=" + userref + ", ptrdId = " + portfolioTradeId + ", trdPair=" + trdPair + ", dir=" + dir + ", marketType=" + marketType
				+ ", price=" + price + ", amount=" + amount + ", orderState=" + orderState + ", orderType=" + orderType
				+ ", errorState=" + errorState + ", sentUnixTime=" + sentUnixTime + ", transIdList=" + transIdList
				+ "]";
	}



	public enum OrderDirection{
		buy,
		sell
	}
	
	public enum MarketType{
		limit,
		market
	}
	
	public enum OrderType{
		NewOrder,
		MoveOrder,
		KillOrder
	}
	
	public enum OrderState{
		Adding,
		Moving,
		Killing,
		
		Active,
		Killed,
		Executed,
		
		Refused
	}
}
