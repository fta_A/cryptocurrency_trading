package connector;

import java.util.ArrayList;

import interfaces.iAddOrderResult;
import interfaces.iTrade;

public class AddOrderResult implements iAddOrderResult {

	protected ArrayList<iTrade> tradesList;
	protected String orderNumber;
	
	@Override
	public String getOrderNumber() {
		// TODO Auto-generated method stub
		return this.orderNumber;
	}

	@Override
	public ArrayList<iTrade> getOrderTrades() {
		// TODO Auto-generated method stub
		return this.tradesList;
	}
	
}
