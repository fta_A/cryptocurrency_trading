package connector;

public class FloodControl {
	private int floodPeriodMlSec;
	private int maxFloodRequests;
	
	private long unfreezeMoment;
	
	
	long[] data;
	int first;
	int data_size;
	
	int DATA_CAPACITY = 100000;
	
	public FloodControl() {
		this.floodPeriodMlSec = 0;
		this.maxFloodRequests = 0;
		
		this.data = new long[DATA_CAPACITY];
		this.first = 0;
		this.data_size = 0;
		
		this.unfreezeMoment = 0;
	}
	
	public FloodControl(int floodPeriodSec, int maxFloodRequests){
		this.floodPeriodMlSec = (int) (floodPeriodSec * 1000 * 1.05);
		this.maxFloodRequests = maxFloodRequests;
		
		this.data = new long[DATA_CAPACITY];
		this.first = 0;
		this.data_size = 0;
	}
	
	public void SetFloodPeriod(int floodPeriodSec) {
//		this.floodPeriodMlSec = (int) (floodPeriodSec * 1000 * 1.05);
		this.floodPeriodMlSec = floodPeriodSec * 1000;
	}
	
	public void SetMaxFloodRequests(int maxFloodRequests) {
		this.maxFloodRequests = maxFloodRequests;
	}
	
	synchronized public int AvailableRequests(){
		long currNanoTime = System.currentTimeMillis();//System.nanoTime();
		if(currNanoTime < this.unfreezeMoment) return 0;

		while(data_size != 0 && ((currNanoTime - data[first]) >= this.floodPeriodMlSec)){
			first = (first + 1) % DATA_CAPACITY;
			data_size--;
		}
		
		return maxFloodRequests - data_size;		
	}
	
	synchronized public void NewRequest(){
		data[(first + data_size) % DATA_CAPACITY] = System.currentTimeMillis();//System.nanoTime();
		data_size++;
	}
	
	synchronized public int GetRequestsForGivenPeriod(int period) {
		int requests = 0;
		
		int position = (first + data_size - 1) % DATA_CAPACITY;
		int size = data_size;
		
		long currNanoTime = data[position];

		while(size != 0 && ((currNanoTime - data[position]) <= period * 1000)){
			position = position - 1;
			if(position < 0) position = DATA_CAPACITY - 1;
			size--;
			requests++;
		}
		
		return requests;
	}
	
	
	synchronized public void PrintLastRequestsStat(int requestsCnt) {
		if(requestsCnt == 0) requestsCnt = this.first;
		
		int position = (first + data_size - 1) % DATA_CAPACITY;
		
		long lastMoment = data[position];
		requestsCnt--;

		long minDiff = Long.MAX_VALUE;
		long maxDiff = 0;
		
		while(requestsCnt > 0){
			position = position - 1;
			if(position < 0) position = DATA_CAPACITY - 1;

			long timeDiff = lastMoment - data[position];
			
			
			if(timeDiff > maxDiff) maxDiff = timeDiff;
			
			if(timeDiff < minDiff) minDiff = timeDiff;
			
			lastMoment = data[position];
			
			requestsCnt--;
		}
		
		System.out.println("Min: " + minDiff + ", Max:" + maxDiff);
		
	}
	
	synchronized public void FreezeAndAdjustFloodPeriod(int freezeMs, int floodPeriodAdjPercent) {
		this.floodPeriodMlSec = (int) (this.floodPeriodMlSec * (1 + (float) floodPeriodAdjPercent / 100));
		this.unfreezeMoment = System.currentTimeMillis() + freezeMs;
		
		System.out.println("FloodPeriod(ms): " + this.floodPeriodMlSec);
	}
}
