package connector;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

import connector.ConnectionState.TypeEnum;
import connector.Order.OrderType;
import interfaces.iBalance;
import interfaces.iConnectionEvent;
import interfaces.iExchangeConnector;
import interfaces.iMarketDataEvent;
import interfaces.iOrderStateEvent;
import interfaces.iTrade;

public class ExchangeConnector implements iExchangeConnector {
	protected Vector<iConnectionEvent> _connectionStateListeners;
	protected Vector<iMarketDataEvent> _marketDataListeners;
	protected Vector<iOrderStateEvent> _orderStateListeners;
	
	HashMap<String, HashMap<Long, iConnectionEvent>> connectionStateListeners;
	HashMap<String, HashMap<Long, iMarketDataEvent>> marketDataListeners;
	HashMap<String, HashMap<Long, iOrderStateEvent>> orderStateListeners;
	
	String connectionString;
	String exchangeCode;

	protected ConnectionStateListener connectionStateListener;
	protected MarketDataListener marketDataListener;
	protected OrderStateListener orderStateListener;
	
	protected Thread connectionStateListenerThread;
	protected Thread marketDataListenerThread;
	protected Thread orderStateListenerThread;

	protected FloodControl floodControl;
	protected int connectionId;
	
	protected String apiKey;
	protected String privateKey;
	
	long uniqueId;
	
	boolean allSubscribed;
	
	public ExchangeConnector(int connectionId, String connectionString) {
		this.connectionId = connectionId;
		this.floodControl = new FloodControl();		
		this.uniqueId = System.currentTimeMillis();
		
		this.allSubscribed = false;
		
		setConnectionString(connectionString);
		
		this.marketDataListeners = new HashMap<>();
	}
	
	@Override
	public String getConnectionString() {
		// TODO Auto-generated method stub
		return this.connectionString;
	}

	
	private void setConnectionString(String connectionString) {
		// TODO Auto-generated method stub
		this.connectionString = connectionString;

		String[] tokens = connectionString.split(";");
		for (String t : tokens) {
			String[] param = t.split("=");
			switch(param[0]) {
			case "FLOOD_PERIOD":
				this.floodControl.SetFloodPeriod(Integer.parseInt(param[1]));
				
				break;
			case "FLOOD_MSG":
				this.floodControl.SetMaxFloodRequests(Integer.parseInt(param[1]));
				
				break;
			case "EXC_CODE":
				this.exchangeCode = param[1];
				break;
			}
		}
		
	}

	@Override
	public String getExchangeCode() {
		// TODO Auto-generated method stub
		return this.exchangeCode;
	}

	

	@Override
	public int getConnectionId() {
		// TODO Auto-generated method stub
		return this.connectionId;
	}

	

	@Override
	public void Connect() {
		// TODO Auto-generated method stub
		
		if(this.connectionStateListenerThread == null) return;
		
		if(!this.connectionStateListenerThread.isAlive()) {
			this.connectionStateListenerThread.setName(this.exchangeCode + ":CONN");
			this.connectionStateListenerThread.start();
	
			if(this.marketDataListener != null) {
				this.marketDataListenerThread.setName(this.exchangeCode + ":MARKET");
				this.marketDataListenerThread.start();
			}
	
			if(this.orderStateListener != null) {
				this.orderStateListenerThread.setName(this.exchangeCode + ":ORDER");
				this.orderStateListenerThread.start();
			}
		}else {
			
		}
	}

	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub
		
//		_connectionStateListeners.clear();
//		_marketDataListeners.clear();
//		_orderStateListeners.clear();
		
		connectionStateListener.Disconnect();		
		if(this.marketDataListener != null) this.marketDataListener.Disconnect();
		if(this.orderStateListener != null) this.orderStateListener.Disconnect();
		
	}
	@Override
	public void AddConnectionEventListener(iConnectionEvent listener) {
		// TODO Auto-generated method stub		
		if (_connectionStateListeners == null)
			_connectionStateListeners = new Vector<iConnectionEvent>();             
		_connectionStateListeners.addElement(listener);		
	}
	
	@Override
	public void AddMarketDataListener(iMarketDataEvent listener) {
		if (_marketDataListeners == null) _marketDataListeners = new Vector<iMarketDataEvent>();             
		_marketDataListeners.addElement(listener);
	}
	
	@Override
	public void AddOrderStateListener(iOrderStateEvent listener) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null)
			_orderStateListeners = new Vector<iOrderStateEvent>();             
		_orderStateListeners.addElement(listener);		
	}



	@Override
	public void ConnectionStateChanged(ConnectionState connectionState) {
		// TODO Auto-generated method stub
		if (_connectionStateListeners == null || _connectionStateListeners.isEmpty()) return;
		
        
        Enumeration<iConnectionEvent> e = _connectionStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iConnectionEvent iConnectionEvent = (iConnectionEvent)e.nextElement();
        	iConnectionEvent.ConnectionStateChanged(connectionState);
        	
        	
        }
	}

	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		
		if(this.allSubscribed) {
			if (_marketDataListeners == null || _marketDataListeners.isEmpty()) return;
			
	        
	        Enumeration<iMarketDataEvent> e = _marketDataListeners.elements();
	        while (e.hasMoreElements())
	        {
	        	iMarketDataEvent iMarketDataEvent = (iMarketDataEvent)e.nextElement();
	        	iMarketDataEvent.MarketDataChanged(connectionId, marketData);
	        }
		}else {
			if(this.marketDataListeners.isEmpty()) return;
			
			for(iMarketDataEvent iMarketDataEvent : this.marketDataListeners.get(marketData.getTrdPair()).values()) {
				iMarketDataEvent.MarketDataChanged(connectionId, marketData);
			}			
		}
	}
	
	//@Override
	public void MarketDataChanged_old(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub
		
		if (_marketDataListeners == null || _marketDataListeners.isEmpty()) return;
		
        
        Enumeration<iMarketDataEvent> e = _marketDataListeners.elements();
        while (e.hasMoreElements())
        {
        	iMarketDataEvent iMarketDataEvent = (iMarketDataEvent)e.nextElement();
        	iMarketDataEvent.MarketDataChanged(connectionId, marketData);
        }
	}

	@Override
	public void OrderStateChanged(Order orderState) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null || _orderStateListeners.isEmpty()) return;		
        
        Enumeration<iOrderStateEvent> e = _orderStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iOrderStateEvent iOrderStateEvent = (iOrderStateEvent)e.nextElement();
        	iOrderStateEvent.OrderStateChanged(orderState);
        }
	}
	
	@Override
	public void AddOrder(Order order) {
		order.orderType = OrderType.NewOrder;
		this.orderStateListener.NewOrder(order);
	}
	
	@Override
	public void KillOrder(Order order) {
		order.orderType = OrderType.KillOrder;
		this.orderStateListener.NewOrder(order);
	}
	

	@Override
	public void Subscribe(String tickerCode) {
		// TODO Auto-generated method stub
		//tickerCode = tickerCode.toLowerCase();
		this.marketDataListener.Subscribe(tickerCode);
	}

	@Override
	public void Subscribe(iMarketDataEvent listener, String tickerCode) {
		// TODO Auto-generated method stub
		//tickerCode = tickerCode.toLowerCase();
		synchronized (this.marketDataListeners) {
			if(this.marketDataListeners.containsKey(tickerCode)) {
				HashMap<Long, iMarketDataEvent> listeners = this.marketDataListeners.get(tickerCode);
				listeners.put(listener.getUniqueID(), listener);
			}
			else {
				HashMap<Long, iMarketDataEvent> listeners = new HashMap<Long, iMarketDataEvent>();
				listeners.put(listener.getUniqueID(), listener);
				this.marketDataListeners.put(tickerCode, listeners);
			}
		}
		
		this.marketDataListener.Subscribe(tickerCode);
	}

	@Override
	public long getUniqueID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void SetApiKey(String apiKey) {
		// TODO Auto-generated method stub
		this.apiKey = apiKey;
		if(this.orderStateListener != null) this.orderStateListener.SetApiKey(apiKey);
		
	}

	@Override
	public void SetPrivateKey(String privateKey) {
		// TODO Auto-generated method stub
		this.privateKey = privateKey;	
		if(this.orderStateListener != null)this.orderStateListener.SetPrivateKey(privateKey);
	}

	@Override
	public void SubscribeAll() {
		// TODO Auto-generated method stub
		this.marketDataListener.SubscribeAll();
		this.allSubscribed = true;
	}

	@Override
	public void GetBallance() {
		// TODO Auto-generated method stub
		this.orderStateListener.RequestBallance();
	}

	@Override
	public void BalanceStateChanged(iBalance balance) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null || _orderStateListeners.isEmpty()) return;		
        
        Enumeration<iOrderStateEvent> e = _orderStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iOrderStateEvent iOrderStateEvent = (iOrderStateEvent)e.nextElement();
        	
        	balance.setExchange(this.exchangeCode);
        	iOrderStateEvent.BalanceStateChanged(balance);
        }
		
	}

	@Override
	public void TradeExecuted(iTrade trade) {
		// TODO Auto-generated method stub

		if (_orderStateListeners == null || _orderStateListeners.isEmpty()) return;		
        
        Enumeration<iOrderStateEvent> e = _orderStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iOrderStateEvent iOrderStateEvent = (iOrderStateEvent)e.nextElement();
        	
        	trade.setExchange(this.exchangeCode);
        	iOrderStateEvent.TradeExecuted(trade);
        }

		
	}

	@Override
	public void GetTrades(String trdPair, String from) {
		// TODO Auto-generated method stub
		
		
		this.orderStateListener.RequestTrades(trdPair, from);
	}

	@Override
	public ConnectionState.StateEnum GetConnectionState(TypeEnum connectionStateType) {
		// TODO Auto-generated method stub
		
		switch(connectionStateType) {
		case MarketData:
			return this.connectionStateListener.marketDataConnectionState.connectionState;
		case OrderState:
			return this.connectionStateListener.orderStateConnectionState.connectionState;
		}
		
		return null;
	}

	@Override
	public void ExceptionIsAraised(String exceptionString) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null || _orderStateListeners.isEmpty()) return;		
        
        Enumeration<iOrderStateEvent> e = _orderStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iOrderStateEvent iOrderStateEvent = (iOrderStateEvent)e.nextElement();
        	
        	
        	iOrderStateEvent.ExceptionIsAraised(exceptionString);
        }
	}

	


}
