package connector;

public class ConnectionState {
	public int connectionId;
	
	public String stateDescription;
	
	public StateEnum connectionState;
	public TypeEnum connectionType;
	
	public enum StateEnum {
		Connected,
		Disconnected,
		Pending
	}
	
	public enum TypeEnum{
		MarketData,
		OrderState
	}
}
