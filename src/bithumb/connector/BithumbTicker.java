package bithumb.connector;

import java.util.ArrayList;
import java.util.Map;

import interfaces.iTicker;

public class BithumbTicker implements iTicker{
	
	
	private String order_currency;
	public ArrayList<Map<String, Double>> bids = new ArrayList<>();
	public ArrayList<Map<String, Double>> asks = new ArrayList<>();
	
	
	public String getOrder_currency() {
		return order_currency;
	}

	public void setOrder_currency(String order_currency) {
		this.order_currency = order_currency;
	}



	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return  bids == null || bids.size() == 0 ? 0 : bids.get(0).get("price");
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return asks == null || asks.size() == 0 ? 0 :  asks.get(0).get("price");
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return order_currency;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		order_currency = id;
	}

}
