package bithumb.connector;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;
import interfaces.iTicker;

public class BithumbMarketDataListener extends MarketDataListener {
	
	
	public BithumbMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://api.bithumb.com/public/orderbook/all");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructMapType(Map.class, String.class, BithumbTicker.class)
			, "data"
		);

	}

	@Override
	protected ArrayList<iTicker> GetMarketDataList(JsonNode jn, JavaType typeReference){
		ArrayList<iTicker> marketDataList = null;
		
		
		@SuppressWarnings("unchecked")
		Map<String, Object> result = mapper.convertValue(jn, Map.class);
		marketDataList = new ArrayList<>();
		String curr = "";
		for (Map.Entry<String, Object> entry : result.entrySet())
		{
		   if(entry.getValue() instanceof LinkedHashMap){
			   iTicker ticker =  mapper.convertValue(entry.getValue(), BithumbTicker.class);
			   ticker.setId(entry.getKey());	
				
			   marketDataList.add(ticker);
		   }else if (entry.getKey() == "payment_currency"){
			   curr = entry.getValue().toString();
		   }
		}

		for(iTicker ticker : marketDataList) {
			ticker.setId(ticker.Id() + curr);
		}
		
		return marketDataList;
	}
	

}
