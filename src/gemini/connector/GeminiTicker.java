package gemini.connector;

import interfaces.iTicker;

public class GeminiTicker implements iTicker{
	private String trdPair;

	public double bid;
	public double ask;
	public Object volume;
	public double last;

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
