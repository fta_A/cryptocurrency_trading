package gemini.connector;



import java.util.ArrayList;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class GeminiMarketDataListener extends MarketDataListener {
	
	
	public GeminiMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetTickersListSource(TickersListType.OneToOne
				, ""
				, "https://api.gemini.com/v1/symbols"
				, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, String.class)
				, ""
				, "https://api.gemini.com/v1/pubticker/"
				, ""
				, mapper.getTypeFactory().constructType(GeminiTicker.class)
				, ""
			);
	
	}

	
	
	

}
