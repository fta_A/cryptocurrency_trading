package gemini.connector;

import connector.ExchangeConnector;

public class GeminiConnector extends ExchangeConnector {

	public GeminiConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new GeminiConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new GeminiMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

	}

}
