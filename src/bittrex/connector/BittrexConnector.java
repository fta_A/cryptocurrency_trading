package bittrex.connector;

import connector.ExchangeConnector;

public class BittrexConnector extends ExchangeConnector {

	public BittrexConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new BittrexConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new BittrexMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

		this.orderStateListener = new BittrexOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);

	}

}
