package bittrex.connector;


import java.util.ArrayList;
import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class BittrexMarketDataListener extends MarketDataListener {
	
	
	public BittrexMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		

		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://bittrex.com/api/v1.1/public/getmarketsummaries");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BittrexTicker.class)
			, "result"
		);
	
	}

	
	
	

}
