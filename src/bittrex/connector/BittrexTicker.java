package bittrex.connector;

import interfaces.iTicker;

public class BittrexTicker implements iTicker{
	private String marketname;
	private double high;
	private double low;
	private double volume;
	private double last;
	private double basevolume;
	private String timestamp;
	private double bid;
	private double ask;
	private Integer openbuyorders;
	private Integer opensellorders;
	private double prevday;
	private String created;

	
	
	public String getMarketname() {
		return marketname;
	}
	public void setMarketname(String marketname) {
		this.marketname = marketname;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getLast() {
		return last;
	}
	public void setLast(double last) {
		this.last = last;
	}
	public double getBasevolume() {
		return basevolume;
	}
	public void setBasevolume(double basevolume) {
		this.basevolume = basevolume;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getAsk() {
		return ask;
	}
	public void setAsk(double ask) {
		this.ask = ask;
	}
	public Integer getOpenbuyorders() {
		return openbuyorders;
	}
	public void setOpenbuyorders(Integer openbuyorders) {
		this.openbuyorders = openbuyorders;
	}
	public Integer getOpensellorders() {
		return opensellorders;
	}
	public void setOpensellorders(Integer opensellorders) {
		this.opensellorders = opensellorders;
	}
	public double getPrevday() {
		return prevday;
	}
	public void setPrevday(double prevday) {
		this.prevday = prevday;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid;
	}
	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask;
	}
	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return marketname;
	}
	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		marketname = id;
	}



}
