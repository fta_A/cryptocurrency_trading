package bittrex.connector;


import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.OrderDirection;
import connector.OrderStateListener;

public class BittrexOrderStateListener  extends OrderStateListener {

	public BittrexOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls("https://bittrex.com/api/v1.1"
				, "/market"
				, ""
				, "GET /account/getbalances"
				, ""
				);
		
		this.SetBodyInPath();
		this.SetSignMethod("HmacSHA512");
	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		String body = "";
		
		
		switch(order.marketType) {
		case market:
			
			//Temporary disabled
			//body = order.dir == OrderDirection.buy ? "/buymarket" : "/sellmarket";
			
			if(order.dir == OrderDirection.buy) {
				body = "/buylimit";
				order.price *= 1.5;
			}else {
				body = "/selllimit";
				order.price = 0;
			}
				
			break;
		case limit:
			body = order.dir == OrderDirection.buy ? "/buylimit" : "/selllimit";
			break;
			
		}
		
		body += "?apikey=" + this.apiKey + "&nonce=" + nonce;
		body += "&market=" + order.trdPair;
		body += "&quantity=" + order.amount;
		body += "&rate=" + order.price;

		return body;
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "?apikey=" + this.apiKey + "&nonce=" + nonce;

		return body;
	}
	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("apisign", signature);
	}
	
	@Override
	protected String GetStringToSign(String timestamp, String method, String path, String body) {
		return this.URLString + path + body;
	}
	
	

}
