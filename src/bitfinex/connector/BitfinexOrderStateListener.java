package bitfinex.connector;


import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.OrderStateListener;

public class BitfinexOrderStateListener  extends OrderStateListener {

	public BitfinexOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls("https://api.bitfinex.com"
				, "POST /v1/order/new"
				, ""
				, "POST /v1/balances"
				, ""
				);
		
		this.SetSignMethod("HmacSHA384");
		this.DisableWriteToBody();
	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		
        
		
		String body = "{"
				+ "\"request\" : \"" + this.addOrderPathString + "\", "
				+ "\"nonce\" : \"" + nonce + "\", "
				//+ "\"options\" : {"

				+ "\"symbol\" : \"" + order.trdPair.toUpperCase() + "\", "
				+ "\"amount\" : \"" + order.amount + "\", "
				+ "\"price\" : \"" + order.price + "\", "
				+ "\"side\" : \"" + order.dir + "\", "
				+ "\"type\" : \"exchange " + order.marketType + "\", "
				//+ "\"ocoorder\" : \"false\", "
				+ "\"exchange\" : \"bitfinex\""
				
				//+ "}"
				+ "}";
		

		return Base64.getEncoder().encodeToString(body.getBytes());
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "{"
				+ "\"request\" : \"" + this.balanceUrl + "\", "
				+ "\"nonce\" : \"" + nonce + "\""
				//+ "\"options\" : {}"
				+ "}";

		return Base64.getEncoder().encodeToString(body.getBytes());
	}

	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("Content-Type", "application/json");
		httpsURLConnection.setRequestProperty("Accept", "application/json");
		httpsURLConnection.setRequestProperty("X-BFX-APIKEY", apiKey);
		httpsURLConnection.setRequestProperty("X-BFX-PAYLOAD", body);
		httpsURLConnection.setRequestProperty("X-BFX-SIGNATURE", signature);		
	}
	
	
	
	
}
