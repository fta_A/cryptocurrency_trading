package bitfinex.connector;

import interfaces.iTicker;

public class BitfinexTicker implements iTicker{

	double mid;
	double bid;
	double ask;
	double last_price;
	double low;
	double high;
	double volume;
	double timestamp;
	String pair;
	
	
	public double getMid() {
		return mid;
	}

	public void setMid(double mid) {
		this.mid = mid;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
		this.ask = ask;
	}

	public double getLast_price() {
		return last_price;
	}

	public void setLast_price(double last_price) {
		this.last_price = last_price;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	public String getPair() {
		return pair;
	}

	public void setPair(String pair) {
		this.pair = pair;
	}

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return this.bid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return this.ask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return this.pair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		this.pair = id;
	}

}
