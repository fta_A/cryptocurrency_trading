package bitfinex.connector;

import connector.ExchangeConnector;

public class BitfinexConnector extends ExchangeConnector {

	public BitfinexConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new BitfinexConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new BitfinexMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

		this.orderStateListener = new BitfinexOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);

	}

}
