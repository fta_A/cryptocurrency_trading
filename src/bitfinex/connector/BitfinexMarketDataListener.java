package bitfinex.connector;


import java.util.ArrayList;
import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class BitfinexMarketDataListener extends MarketDataListener {
	
	
	public BitfinexMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		

		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://api.bitfinex.com/v1/tickers");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BitfinexTicker.class)
			, ""
		);

	}

	
	
	

}
