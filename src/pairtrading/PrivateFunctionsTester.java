package pairtrading;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Hex;

import connector.Order;

public class PrivateFunctionsTester {
	String bitapiKey = "9EFxvTEwrP91D58aqhd5pg";
	String bitprivateKey = "UuZs3kMUE94HCnRFqp1Pj7mqHaezB4OyuASaNd9FaFE=";

	
	public void NewBitflyerOrder(Order order) {
		
		String path = "/v1/me/sendchildorder";
		String method = "POST";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String body = "{"
				+ "\"product_code\" : \"" + order.trdPair + "\", "
				+ "\"child_order_type\" : \"" + order.marketType.toString().toUpperCase() + "\", "
				+ "\"side\" : \"" + order.dir.toString().toUpperCase() + "\", "
				+ "\"price\" : " + order.price + ", "
				+ "\"size\" : " + order.amount 
				+ "}";
		
		String text = timestamp + method + path + body;
		String sign = calculateSignature(bitprivateKey, "HmacSHA256", text);
		
		

		try {
			URL postURL = new URL("https://api.bitflyer.com" + path);
			
			HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
			httpsURLConnection.setRequestMethod(method);
			httpsURLConnection.setRequestProperty("ACCESS-KEY", bitapiKey);
			httpsURLConnection.setRequestProperty("ACCESS-TIMESTAMP", timestamp);
			httpsURLConnection.setRequestProperty("ACCESS-SIGN", sign);
			httpsURLConnection.setRequestProperty("Content-Type", "application/json");
			httpsURLConnection.setDoOutput(true);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        writer.write(body);
	        writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();
	        
	        System.out.println("Add Order Res: " + jsonString);
	        
	        
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void CheckOpenedOrders(HashMap<Integer, Order> activeOrdersMap) {

		
		
		String path = "/v1/me/getchildorders";
		String method = "GET";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		int first = 1;

		for(Order order : activeOrdersMap.values()) {
			if(first == 1) {
				//path += "?product_code=" + order.trdPair;
				path += "?child_order_acceptance_id=" + order.transIdList.get(0);
				first = 0;
			}else
				//path += "&product_code=" + order.trdPair;
			
			path += "&child_order_acceptance_id=" + order.transIdList.get(0);
		}
		String body = "";
		
		
		String text = timestamp + method + path + body;
		String sign = calculateSignature(bitprivateKey, "HmacSHA256", text);
		
		

		try {
			URL postURL = new URL("https://api.bitflyer.com" + path);
			
			HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
			httpsURLConnection.setRequestMethod(method);
			httpsURLConnection.setRequestProperty("ACCESS-KEY", bitapiKey);
			httpsURLConnection.setRequestProperty("ACCESS-TIMESTAMP", timestamp);
			httpsURLConnection.setRequestProperty("ACCESS-SIGN", sign);
			httpsURLConnection.setRequestProperty("Content-Type", "application/json");
			httpsURLConnection.setDoOutput(true);
			
			//OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        //writer.write(body);
	        //writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();
	        
	        System.out.println("Add Order Res: " + jsonString);
	        
	        
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String calculateSignature(String privateKey, String shaInstance, String postData) {
	    String signature = "";
	    try {
	        Mac hmacSHA256 = Mac.getInstance(shaInstance);
	        SecretKeySpec secret_key = new SecretKeySpec(privateKey.getBytes(), shaInstance);
	        hmacSHA256.init(secret_key);
	        signature = Hex.encodeHexString(hmacSHA256.doFinal(postData.getBytes()));

	    } catch(Exception e) {
	    	
	    }
	    
	    return signature;		
	}
	
}
