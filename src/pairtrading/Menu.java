package pairtrading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import connector.Balance;

public class Menu {
    private final String name;
    private final String text;
    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    public Menu(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public void putAction(String name, Runnable action) {
        actionsMap.put(name, action);
    }

    public String generateText() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(": ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    public void executeAction(int actionNumber) {
        int effectiveActionNumber = actionNumber - 1;
        if (effectiveActionNumber < 0 || effectiveActionNumber >= actionsMap.size()) {
            System.out.println("Ignoring menu choice: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(effectiveActionNumber).run();
        }
    }

    public static class App {
        private Menu menu;
        HashMap<Integer, Portfolio> portfolios;
        HashMap<Integer, Balance> balances;
        
        Portfolio portfolio = null;

        public App(HashMap<Integer, Portfolio> portfolios, HashMap<Integer, Balance> balances) {
        	this.portfolios = portfolios;
        	this.balances = balances;
        	
            Menu mainMenu = new Menu("Main", "main menu");
            Menu subMenuGetPassword = new Menu("Portfolios", "select, buy, sell");

            mainMenu.putAction("get portfolios menu", () -> activateMenu(subMenuGetPassword));
            mainMenu.putAction("quit", () -> System.exit(0));

            
            subMenuGetPassword.putAction("show all portfolios", () -> showAllPortfolios());
            subMenuGetPassword.putAction("show all balances", () -> showBalances());
            subMenuGetPassword.putAction("select portfolio", () -> setActivePortfolio());
            subMenuGetPassword.putAction("show active portfolio", () -> showActivePortfolio());
            subMenuGetPassword.putAction("buy active portfolio", () -> tradeActivePortfolio(1));
            subMenuGetPassword.putAction("sell active portfolio", () -> tradeActivePortfolio(0));
            subMenuGetPassword.putAction("quit", () -> System.exit(0));

           

            activateMenu(mainMenu);            
        }

        private void activateMenu(Menu newMenu) {
            menu = newMenu;
            System.out.println(newMenu.generateText());
            @SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
            while (true) {
                // TODO some error checking.
                int actionNumber = scanner.nextInt();
                menu.executeAction(actionNumber);
            }
        }

        private void showAllPortfolios() {
        	for(Portfolio portfolio : portfolios.values()) {
        		System.out.println(portfolio.toString());
        	}
        }
        
        private void setActivePortfolio() {
        	System.out.println("Select portfolio_id:");
        	@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
        	
            int actionNumber = scanner.nextInt();
            portfolio = portfolios.get(actionNumber);
            
        }
        
        private void showActivePortfolio() {
        	if(portfolio == null) return;
        	System.out.println(portfolio.toString());
        }
        
        private void tradeActivePortfolio(int dir) {
        	if(portfolio == null) return;
        	if(dir == 1) {
        		portfolio.buy = 1;
        		portfolio.sell = 0;
        		portfolio.buy_at = portfolio.wask;
        		
        		portfolio.trade = 1;
        	}else {
        		portfolio.buy = 0;
        		portfolio.sell = 1;
        		portfolio.sell_at = portfolio.wbid;
        		
        		portfolio.trade = 1;
       		
        	}
        }
        
        private void showBalances() {
        	for(Balance balance : balances.values()) {
        		if(balance.getAmount() != 0)
        		System.out.println(balance.toString());
        	}
       	
        }
        
     }

}