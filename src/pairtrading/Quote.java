package pairtrading;

import interfaces.iBalance;
import interfaces.iExchangeConnector;

public class Quote {
	public int quote_id;
	public int portfolio_id;
	
	public String ticker_code;

	public int isin_id;

	public int weight;

	public double wbid;
	public double wask;

	public double ask_tgt;
	public double bid_tgt;

	public double pt_value;

	public double price_scale;
	public double ptv_scale;

	public double minstep;

	public char is_left;	
	public char qnt_type;
	
	public Portfolio portfolio;
	public iExchangeConnector exchangeConnector;
	public int connection_id;
	
	public int money_balance_id;
	public int secur_balance_id;
	public iBalance securBalance;
	public iBalance moneyBalance;

	
	
	public int is_quanto;
	public int is_quanto_mltp;
	public String quanto_ticker_code;
	public int quanto_connection_id;
	public iExchangeConnector quanto_exchangeConnector;

	public int quanto_money_balance_id;
	public int quanto_secur_balance_id;
	public iBalance quanto_securBalance;
	public iBalance quanto_moneyBalance;
	
	
	public double quanto_bid;
	public double quanto_ask;
	
	
	//public struct isin_data_tbl_t *isin_data;
}
