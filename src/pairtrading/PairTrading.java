package pairtrading;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import connector.Balance;
import connector.ConnectionState;
import connector.ConnectionState.StateEnum;
import connector.MarketData;
import connector.Order;
import connector.Order.MarketType;
import connector.Order.OrderDirection;
import connector.Order.OrderState;
import connector.PortfolioTrade;
import interfaces.iBalance;
import interfaces.iConnectionEvent;
import interfaces.iExchangeConnector;
import interfaces.iMarketDataEvent;
import interfaces.iOrderStateEvent;
import interfaces.iTrade;

public class PairTrading  implements iConnectionEvent, iMarketDataEvent, iOrderStateEvent, Runnable {
	
	HashMap<Integer, Integer> usedConnections;
	HashMap<Integer, iExchangeConnector> exchangeConnectors;
	HashMap<String, Integer> exchangesMap;
	
	HashMap<Integer, Portfolio> portfolios;
	HashMap<String, ArrayList<Quote>> quotesMap;
	HashMap<String, ArrayList<Quote>> quantoMap;	
	HashMap<Integer, ArrayList<Quote>> quoteConnectors;
	

	int orderId = (int) (System.currentTimeMillis() - 1509494400000L) / 1000;
	
	
	HashMap<Integer, Order> activeOrdersMap = new HashMap<Integer, Order>();

	HashMap<Integer, Balance> balancesMap = new HashMap<>();
	HashMap<String, Balance> balancesMapByExchCurr = new HashMap<>();
	
	HashMap<String, iBalance> unsycBalancesMap = new HashMap<>();

	PreparedStatement updatePortfoliosPreparedStatement = null;
	PreparedStatement newTradePreparedStatement = null;
	PreparedStatement balancePreparedStatement = null;
	PreparedStatement reqBalancePreparedStatement = null;
	PreparedStatement portfoliosJSONStatement = null;
	PreparedStatement balancesJSONStatement = null;
	PreparedStatement sysStateStatement = null;
	PreparedStatement ordersToExecuteStatement = null;
	PreparedStatement lastOrderToExecuteStatement = null;
	PreparedStatement exchangesStatement = null;
	PreparedStatement newPortfolioTradeStatement = null;
	PreparedStatement newExceptionStatement = null;

	String updatePortfoliosQuery = "update dbo.portfolios set amount = ?, buy_at = ?, sell_at = ? where portfolio_id = ?";
	
	String dbInstertTradeQuery = "insert into dbo.trades_tmp (exchange_code, ticker_code, trade_id, unix_utctimestamp, price, amount, fee, dir, order_id, portfolio_trade_id, userref_id)"
			+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	String dbInsertBalanceQuery = "insert into dbo.balances_tmp (exchange_code, currency_code, amount, available, unix_utctimestamp)"
			+ " values (?, ?, ?, ?, ?)";

	String dbInsertReqBalanceQuery = "insert into dbo.req_balances (portfolio_id, balance_id, amount, unix_utctimestamp)"
			+ " values (?, ?, ?, ?)";

	
	String dbGetPortfoliosJSONQuery = "select dbo.fnGetPortfoliosJSON()";
	
	String dbGetBalancesJSONQuery = "select dbo.fnGetBalancesJSON()";

	String dbGetExchangesJSONQuery = "select dbo.fnGetExchangesJSON()";
	
	String getOrdersToExecuteQuery = "select exchange_code_id, ticker_code, amount, price, order_to_execute_id from dbo.fnGetOrdersToExecute(?)";
	
	String getLastOrderToExecuteQuery = "select isnull(max(order_to_execute_id),0) from dbo.orders_to_execute";
	
	String dbGetSystemState = "select description, state, last_modified from dbo.sys_states where last_modified > ?";
	
	String newPortfolioTradeQuery = "insert into dbo.prtf_trades (portfolio_id, portfolio_trade_id, amount, price)"
			+ " values (?, ?, ?, ?)"; 
	
	String newExceptionQuery = "insert into dbo.exceptions (description, moment) values (?, ?)";
	
	Long systemStateMoment = (long) 0;
	
	ArrayList<iBalance> balancesToSaveList = new ArrayList<>();
	ArrayList<iBalance> balancesToSaveListSwap = new ArrayList<>();
	
	ArrayList<iTrade> tradesToSaveList = new ArrayList<>();
	ArrayList<iTrade> tradesToSaveListSwap = new ArrayList<>();
	
	HashSet<PortfolioTrade> portfoliosToSaveSet = new HashSet<>();
	HashSet<PortfolioTrade> portfoliosToSaveSetSwap = new HashSet<>();
	
	ArrayList<String> exceptionsToSaveList = new ArrayList<>();
	ArrayList<String> exceptionsToSaveListSwap = new ArrayList<>();

	
	
	boolean system_started = false;
	
	Thread houseKeeperThread;
	
	String connectionsPath;
	
	long portfolioTradeId = System.currentTimeMillis();	
	
	long lastUpdatedRequest = 0;
	
	int lastOrderToExecute = 0;
	
	public PairTrading(String connectionsPath) {
		exchangeConnectors = new HashMap<Integer, iExchangeConnector>();
		usedConnections = new HashMap<>();
		
		exchangesMap = new HashMap<String, Integer>();

		portfolios = new HashMap<Integer, Portfolio>();
		quotesMap = new HashMap<>();
		quantoMap = new HashMap<>();
		quoteConnectors = new HashMap<>();
		
		this.houseKeeperThread = new Thread(this);
		
		this.connectionsPath = connectionsPath;
		
	//	if(orderId < 0) System.out.println("OrderId = " + orderId);
	}
	
	public void Start() {
		this.InitTradingParameters();
		

		for(iExchangeConnector connector : this.exchangeConnectors.values())
			connector.Connect();
	
		
		this.houseKeeperThread.start();
		
		new Menu.App(this.portfolios, this.balancesMap);
		
		
	}
	
	
	
	
	
	void CheckSystemState() {
		
		try {
			sysStateStatement.setLong(1, systemStateMoment);
			
			
			ResultSet resultSet = sysStateStatement.executeQuery();
			
			
			
			while(resultSet.next()) {
				String cmd = resultSet.getString(1);
				long updateMoment = resultSet.getLong(3);		
				int state = resultSet.getInt(2);
			
				switch(cmd) {
				case "TradeState":{
					if(state == 1) this.system_started = true;
					else this.system_started = false;
				}
				break;
				
				case "RequestBalances":{
					RequestBalances();
				}break;
				
				case "SyncBalances":{
					SyncBalances();
				}break;
				
				case "ExecuteRequestedOrders":{
					ExecuteRequestedOrders();
				}break;
				
				case "UpdatePortfolios":{
					if(state == 1) {
						boolean currState = this.system_started;
						this.system_started = false;
						
						String jsonPortfolios = GetJSONStringFromPreparedStatement(this.portfoliosJSONStatement);
						String jsonBalances = GetJSONStringFromPreparedStatement(this.balancesJSONStatement);
						
						synchronized (this) { 
							InitBalances(jsonBalances);
							InitPortfolios(jsonPortfolios);
						}
						
						
						this.system_started = currState;
								
					}
									
				}break;
				}
				
				if(updateMoment > this.systemStateMoment) this.systemStateMoment = updateMoment;
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}
	
	void ExecuteRequestedOrders() {
		try {
			ordersToExecuteStatement.setInt(1, this.lastOrderToExecute);
			
			//exchange_code_id, ticker_code, amount, price, order_to_execute_id
			
			ResultSet resultSet = ordersToExecuteStatement.executeQuery();			
			while(resultSet.next()) {
				int lastOrdToExec = resultSet.getInt(5);
				
				iExchangeConnector iExchange = this.exchangeConnectors.get(resultSet.getInt(1));
				if(iExchange != null) {
					Order order = new Order();
					order.amount = resultSet.getDouble(3);
					order.price = resultSet.getDouble(4);
					order.trdPair = resultSet.getString(2);
					
					if(order.amount > 0) order.dir = OrderDirection.buy;
					else {
						order.dir = OrderDirection.sell;
						order.amount = -order.amount;
					}
					
					order.marketType = MarketType.market;
					order.userref = orderId++;
					
					iExchange.AddOrder(order);
				}
				
				
				if(lastOrdToExec > this.lastOrderToExecute) this.lastOrderToExecute = lastOrdToExec; 
			}			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	void GetInitialSystemState() {
		try {
			sysStateStatement.setLong(1, systemStateMoment);
			
			
			ResultSet resultSet = sysStateStatement.executeQuery();
						
			while(resultSet.next()) {
				long updateMoment = resultSet.getLong(3);
				if(updateMoment > this.systemStateMoment) this.systemStateMoment = updateMoment;
			}
			
			
			
			resultSet = lastOrderToExecuteStatement.executeQuery();			
			while(resultSet.next()) {
				this.lastOrderToExecute = resultSet.getInt(1);
			}			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String jsonPortfolios = GetJSONStringFromPreparedStatement(this.portfoliosJSONStatement);
		String jsonBalances = GetJSONStringFromPreparedStatement(this.balancesJSONStatement);
		
		synchronized (this) { 
			InitBalances(jsonBalances);
			InitPortfolios(jsonPortfolios);
		}
	}
	
	
	void SyncBalances() {
		Iterator<Entry<String, iBalance>> it = unsycBalancesMap.entrySet().iterator();
	    while (it.hasNext()) {
	    	Entry<String, iBalance> pair = it.next();
	        
			Balance balance = balancesMapByExchCurr.get(pair.getKey());
			
	        if(balance != null) {
		        balance.setAmount(pair.getValue().Availlable());
				balance.setBalanceValue(pair.getValue().BalanceValue());
	        }
	        
	        it.remove(); // avoids a ConcurrentModificationException
	    }
		
		
	}
	
	void RequestBalances() {		
		for(iExchangeConnector iExchangeConnector : exchangeConnectors.values())
			iExchangeConnector.GetBallance();			
	}
	
	void InitTradingParameters() {
		
		
		
		InitDBStorage();
		InitExchanges();		
		InitConnections();
		
		GetInitialSystemState();
		
		//CheckSystemState();
		
		//this.system_started = false;
	}
	
	private void InitExchanges() {
		String jsonExchanges = GetJSONStringFromPreparedStatement(this.exchangesStatement);
		
		ObjectMapper mapper = new ObjectMapper();
		
		exchangesMap.clear();
		
		try {
			JsonNode jn = mapper.readTree(jsonExchanges);
	        TypeReference<ArrayList<ExchConnection>> listTypeReference = new TypeReference<ArrayList<ExchConnection>> () {};
	        ArrayList<ExchConnection> list = mapper.convertValue(jn, listTypeReference);
	        for(ExchConnection item : list) {
	        	exchangesMap.put(item.getExchange_code(), item.getExchange_code_id());
	        }
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	        	
	}
	
	
	private String GetJSONStringFromPreparedStatement(PreparedStatement preparedStatement) {
		String jsonString = null;
		
		
		try {
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			jsonString = resultSet.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonString;
	}
	
	private String GetFileContents(String path) {
		
		try {
			Scanner in = new Scanner(new FileReader(path));
			StringBuilder sb = new StringBuilder();
			while(in.hasNext()) {
			    sb.append(in.next());
			}
			in.close();
			
			return sb.toString();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private void InitDBStorage() {
		String jsonStorage = GetFileContents(this.connectionsPath);
		if(jsonStorage == null) {
			System.out.println("Connections file is missed. Exiting.");
			System.exit(0);
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			JsonNode jn = mapper.readTree(jsonStorage);
			jn = jn.get("dbstorage");
					
			if(jn == null) {
				System.out.println("Connections file has wrong JSON structure. Exiting.");
				System.exit(0);
			}
			
			
			DBConnector dbconn = mapper.convertValue(jn, DBConnector.class);
			
			Class.forName(dbconn.jdbc_driver);
			Connection conn = DriverManager.getConnection(dbconn.db_url, dbconn.user, dbconn.pass);
			newTradePreparedStatement = conn.prepareStatement(dbInstertTradeQuery);
			balancePreparedStatement = conn.prepareStatement(dbInsertBalanceQuery);
			reqBalancePreparedStatement = conn.prepareStatement(dbInsertReqBalanceQuery);
			
			portfoliosJSONStatement = conn.prepareStatement(dbGetPortfoliosJSONQuery);
			balancesJSONStatement = conn.prepareStatement(dbGetBalancesJSONQuery);
			sysStateStatement = conn.prepareStatement(dbGetSystemState);
			
			updatePortfoliosPreparedStatement = conn.prepareStatement(updatePortfoliosQuery);
			exchangesStatement = conn.prepareStatement(dbGetExchangesJSONQuery);
			newPortfolioTradeStatement = conn.prepareStatement(newPortfolioTradeQuery);
			
			ordersToExecuteStatement = conn.prepareStatement(getOrdersToExecuteQuery);
			lastOrderToExecuteStatement = conn.prepareStatement(getLastOrderToExecuteQuery);
			
			newExceptionStatement = conn.prepareStatement(newExceptionQuery);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void InitConnections() {
		
		String jsonStorage = GetFileContents(this.connectionsPath);
		if(jsonStorage == null) {
			System.out.println("Connections file is missed. Exiting.");
			System.exit(0);
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			JsonNode jn = mapper.readTree(jsonStorage);
			jn = jn.get("exchanges");
					
			if(jn == null) {
				System.out.println("Connections file has wrong JSON structure. Exiting.");
				System.exit(0);
			}
			
			
	        TypeReference<ArrayList<ExchConnection>> listTypeReference = new TypeReference<ArrayList<ExchConnection>> () {};
	        ArrayList<ExchConnection> list = mapper.convertValue(jn, listTypeReference);

	        for(ExchConnection conn : list) {
	        	try {
	        		if(!exchangesMap.containsKey(conn.exchange_code)) continue;
	        		
	        		int exchange_code_id = exchangesMap.get(conn.exchange_code);
	        		
					Class<?> cl = Class.forName(conn.getClass_id());
					Constructor<?> con = cl.getConstructor(int.class, String.class);
					
					iExchangeConnector iExchangeConnector = (iExchangeConnector) con.newInstance(exchange_code_id, conn.getConnection_string());
					
					iExchangeConnector.SetApiKey(conn.getApi_key());
					iExchangeConnector.SetPrivateKey(conn.getPrivate_key());
					
					iExchangeConnector.AddMarketDataListener(this);
					iExchangeConnector.AddOrderStateListener(this);
					iExchangeConnector.AddConnectionEventListener(this);
					
					iExchangeConnector.GetBallance();
					//iExchangeConnector.GetTrades("", "1");
					
					
					
					exchangeConnectors.put(exchange_code_id, iExchangeConnector);
					usedConnections.put(exchange_code_id, 0);
					
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	private void InitBalances(String balancesString) {
		balancesMap.clear();
		balancesMapByExchCurr.clear();
		
		if(balancesString == null) return;
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode jn = mapper.readTree(balancesString);
	        TypeReference<ArrayList<Balance>> listTypeReference = new TypeReference<ArrayList<Balance>> () {};
	        ArrayList<Balance> list = mapper.convertValue(jn, listTypeReference);
	        
	        for(Balance balance : list) {
	        	balancesMap.put(balance.BalanceId(), balance);

	        	balancesMapByExchCurr.put(balance.Exchange() + balance.CurrencyCode(), balance);
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	
	private void InitPortfolios(String jsonPortfolios) {
		ObjectMapper mapper = new ObjectMapper();
		
		portfolios.clear();
		quotesMap.clear();
		quantoMap.clear();
		quoteConnectors.clear();
		
		try {
			JsonNode jn = mapper.readTree(jsonPortfolios);
	        TypeReference<ArrayList<Portfolio>> listTypeReference = new TypeReference<ArrayList<Portfolio>> () {};
	        ArrayList<Portfolio> list = mapper.convertValue(jn, listTypeReference);
	        
	        for(Portfolio portfolio : list) {
	        	portfolios.put(portfolio.portfolio_id, portfolio);
	        
	        	 for(Quote quote : portfolio.quotes_tbl) {
	 	        	iExchangeConnector iExchangeConnector = this.exchangeConnectors.get(quote.connection_id);
	 	        	
	 	        	quote.portfolio = portfolio;
	 	        	quote.exchangeConnector = iExchangeConnector;
	 	        	
	 	        	String key = quote.connection_id + ":" + quote.ticker_code;
	 	        	//portfolio.quotes_tbl.put(key, quote);
	 	        	portfolio.zero_wasks_num++;
	 	        	portfolio.zero_wbids_num++;
	 	        	
	 	        	
	 	        	if(quotesMap.containsKey(key))quotesMap.get(key).add(quote);
	 	        	else {
	 	        		ArrayList<Quote> qList = new ArrayList<>();
	 	        		qList.add(quote);
	 	        		quotesMap.put(key, qList);
	 	        	}
	 	        	
	 	        	if(quote.exchangeConnector != null) quote.exchangeConnector.Subscribe(this, quote.ticker_code);
	 	        	
	 	        	if(quoteConnectors.containsKey(quote.connection_id))quoteConnectors.get(quote.connection_id).add(quote);
	 	        	else {
	 	        		ArrayList<Quote> qList = new ArrayList<>();
	 	        		qList.add(quote);
	 	        		
	 	        		quoteConnectors.put(quote.connection_id, qList);	 	        		
	 	        	}
	 	        	
	 	        	if(quote.exchangeConnector != null) {
		 	        	if(quote.exchangeConnector.GetConnectionState(ConnectionState.TypeEnum.MarketData) != StateEnum.Connected) 
		 	        		portfolio.dead_connectors ++;
		 	        	
		 	        	if(quote.exchangeConnector.GetConnectionState(ConnectionState.TypeEnum.OrderState) != StateEnum.Connected) 
		 	        		portfolio.dead_connectors ++;
	 	        	}else {
	 	        		portfolio.dead_connectors += 2;
	 	        	}
	 	        	
	 	        	quote.moneyBalance = this.balancesMap.get(quote.money_balance_id);
	 	        	quote.securBalance = this.balancesMap.get(quote.secur_balance_id);
	 	        	
	 	        	portfolio.neededBalances.put(quote.moneyBalance.BalanceId(), new Balance(quote.moneyBalance));
	 	        	portfolio.neededBalances.put(quote.securBalance.BalanceId(), new Balance(quote.securBalance));
	 	        	
	 	        	
	 	        	if(quote.is_quanto == 1) {
	 	        		key = quote.quanto_connection_id + ":" + quote.quanto_ticker_code;
		 	        	//portfolio.quotes_tbl.put(key, quote);
		 	        	portfolio.zero_wasks_num++;
		 	        	portfolio.zero_wbids_num++;
		 	        	
		 	        	quote.quanto_bid = 0;
		 	        	quote.quanto_ask = 0;
		 	        	
		 	        	if(quantoMap.containsKey(key))quantoMap.get(key).add(quote);
		 	        	else {
		 	        		ArrayList<Quote> qList = new ArrayList<>();
		 	        		qList.add(quote);
		 	        		quantoMap.put(key, qList);
		 	        	}
		 	        	
		 	        	iExchangeConnector = this.exchangeConnectors.get(quote.quanto_connection_id);
		 	        	quote.quanto_exchangeConnector = iExchangeConnector;
		 	        	
		 	        	if(quote.quanto_exchangeConnector != null) quote.quanto_exchangeConnector.Subscribe(this, quote.quanto_ticker_code);
		 	        	
		 	        	
		 	        	if(quoteConnectors.containsKey(quote.quanto_connection_id))quoteConnectors.get(quote.quanto_connection_id).add(quote);
		 	        	else {
		 	        		ArrayList<Quote> qList = new ArrayList<>();
		 	        		qList.add(quote);
		 	        		
		 	        		quoteConnectors.put(quote.quanto_connection_id, qList);	 	        		
		 	        	}	 	        	

		 	        	if(quote.quanto_exchangeConnector != null) {
			 	        	if(quote.quanto_exchangeConnector.GetConnectionState(ConnectionState.TypeEnum.MarketData) != StateEnum.Connected) 
			 	        		portfolio.dead_connectors ++;
			 	        	
			 	        	if(quote.quanto_exchangeConnector.GetConnectionState(ConnectionState.TypeEnum.OrderState) != StateEnum.Connected) 
			 	        		portfolio.dead_connectors ++;
		 	        	}else {
		 	        		portfolio.dead_connectors += 2;
		 	        	}
		 	        	
		 	        	quote.quanto_moneyBalance = this.balancesMap.get(quote.quanto_money_balance_id);
		 	        	quote.quanto_securBalance = this.balancesMap.get(quote.quanto_secur_balance_id);

		 	        
		 	        	portfolio.neededBalances.put(quote.quanto_moneyBalance.BalanceId(), new Balance(quote.quanto_moneyBalance));
		 	        	portfolio.neededBalances.put(quote.quanto_securBalance.BalanceId(), new Balance(quote.quanto_securBalance));

		 	        	
	 	        	}else {
		 	        	quote.quanto_bid = 1;
		 	        	quote.quanto_ask = 1;
	 	        	}
	 	        }
	        	
	        	
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	@Override
	public long getUniqueID() {
		// TODO Auto-generated method stub
		return 0;
	}



	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub
		
		synchronized (this) {
			String key = connectionId + ":" + marketData.getTrdPair();
			//System.out.print(key);
			
			ArrayList<Order> ordersToSend = new ArrayList<>();
			
			ArrayList<Quote> qList = quotesMap.get(key);
			if(qList != null) {
				for(Quote quote : qList) {
					QuoteChanged(quote, marketData);			
					CheckPortfolio(quote.portfolio, ordersToSend);
				}
			}
			
			qList = quantoMap.get(key);
			if(qList != null) {
				for(Quote quote : qList) {
					QuantoQuoteChanged(quote, marketData);			
					CheckPortfolio(quote.portfolio, ordersToSend);
				}
			}
			
			/*
			for(Order order : ordersToSend) {
				Quote quote = (Quote) order.parent;
				Portfolio portfolio = quote.portfolio;
				portfolio.active_orders++;
						
				
				if(quote.ticker_code == order.trdPair) AdjustTradingBalance(order, quote.securBalance, quote.moneyBalance);
				else AdjustTradingBalance(order, quote.quanto_securBalance, quote.quanto_moneyBalance);
				
				synchronized (activeOrdersMap) {
					activeOrdersMap.put(order.userref, order);
				}
				order.exchangeConnector.AddOrder(order);
			}
			
			if(ordersToSend.size() > 3)
				System.out.println("-----MULTIPLE ORDERS TO SEND: " + ordersToSend.size());
			*/
		}
	}

	private void CheckPortfolio(Portfolio portfolio, ArrayList<Order> ordersToSend) {

		if(!system_started) return;
		
		if(portfolio.trade == 0
			|| portfolio.active_orders > 0
			|| portfolio.dead_connectors > 0
			|| portfolio.used_connectors > 0
		) return;
		
		
		
		double trd_amt = 0;
		
		
		if(portfolio.buy == 1 
			&& portfolio.zero_wasks_num == 0
			&& portfolio.buy_at > portfolio.wask
			&& portfolio.amount < portfolio.buy_limit
			) {			
			
			trd_amt = portfolio.trd_amt;
			
			
			
		}else if(portfolio.sell == 1 
				&& portfolio.zero_wbids_num == 0
				&& portfolio.sell_at < portfolio.wbid
				&& portfolio.amount > portfolio.sell_limit
				) {
			
			
			trd_amt = -portfolio.trd_amt;
		}

		ArrayList<Order> portfolioOrders = new ArrayList<>();
		boolean trade = false;
		
		if(this.portfolioTradeId >= System.currentTimeMillis()) this.portfolioTradeId++;
		else this.portfolioTradeId = System.currentTimeMillis();
		
		if(trd_amt != 0) {
			trade = true;
			boolean portfolio_buy = trd_amt > 0 ? true : false;
			
			for(Quote quote : portfolio.quotes_tbl) {
				Order order = CreateTradeOrder(quote, trd_amt);	
				order.portfolioTradeId = portfolioTradeId;
				
				
				if(CheckTradingBalance(portfolio_buy, order, quote.securBalance, quote.moneyBalance))
					portfolioOrders.add(order);
				else {
					trade = false;
					//break;
				}
				
				if(quote.is_quanto == 1) {
					order = CreateTradeQuantoOrder(quote, trd_amt);
					order.portfolioTradeId = portfolioTradeId;
					
					if(CheckTradingBalance(portfolio_buy, order, quote.quanto_securBalance, quote.quanto_moneyBalance))
						portfolioOrders.add(order);
					else {
						trade = false;
						//break;
					}					
				}
			}
		}
		
		if(trade) {
			//system_started = false;
			
			for(Order order : portfolioOrders) {
				//ordersToSend.add(order);

				Quote quote = (Quote) order.parent;
				
				portfolio.active_orders++;
				this.usedConnections.put(quote.connection_id, this.usedConnections.get(quote.connection_id) + 1);
				
				
				if(quote.ticker_code == order.trdPair) AdjustTradingBalance(order, quote.securBalance, quote.moneyBalance);
				else AdjustTradingBalance(order, quote.quanto_securBalance, quote.quanto_moneyBalance);
				
				synchronized (activeOrdersMap) {
					activeOrdersMap.put(order.userref, order);
				}
				order.exchangeConnector.AddOrder(order);

			}
			
			PortfolioTrade portfolioTrade = new PortfolioTrade();
			portfolioTrade.portfolio_id = portfolio.portfolio_id;
			portfolioTrade.portfolio_trade_id = portfolioTradeId;
			portfolioTrade.trd_amt = trd_amt;
			portfolioTrade.price = trd_amt > 0 ? portfolio.wask : portfolio.wbid;
			
			portfolio.amount += trd_amt;
			if(trd_amt > 0) {
				//System.out.println("New Portfolio Trade:{" + portfolio.portfolio_id + "} Buy, " + portfolio.wask);
				portfolio.buy_at -= portfolio.shift;
				portfolio.sell_at -= portfolio.shift;
			}else {
				//System.out.println("New Portfolio Trade:{" + portfolio.portfolio_id + "} Sell, " + portfolio.wbid);
				portfolio.buy_at += portfolio.shift;
				portfolio.sell_at += portfolio.shift;
			}
			
			PortfolioDataChanged(portfolioTrade);
			
		}
			
	}
	
	
	private Order CreateTradeOrder(Quote quote, double trd_amt) {		
		OrderDirection dir = OrderDirection.buy;
		if(Math.signum(quote.weight) * Math.signum(trd_amt) < 0) dir = OrderDirection.sell;
		
		Order order = new Order();
		order.trdPair = quote.ticker_code;
		order.dir = dir;
		order.amount =	Math.abs(trd_amt);
		order.price = dir == OrderDirection.buy ? quote.ask_tgt : quote.bid_tgt;
		order.marketType = MarketType.market;
		order.userref = orderId++;
		order.orderState = OrderState.Adding;
		order.exchangeConnector = quote.exchangeConnector;
		order.parent = quote;
		
		return order;
	}
	
	private Order CreateTradeQuantoOrder(Quote quote, double trd_amt) {		
		OrderDirection dir = OrderDirection.buy;
		OrderDirection oppDir = OrderDirection.sell;
		
		if(Math.signum(quote.weight) * Math.signum(trd_amt) < 0) {
			dir = OrderDirection.sell;
			oppDir = OrderDirection.buy;;
		}
		
		Order order = new Order();
		order.trdPair = quote.quanto_ticker_code;
		
		if(quote.is_quanto_mltp == 1) {
			order.dir = dir;
			if(dir == OrderDirection.buy) {				
				order.amount = quote.ask_tgt * Math.abs(trd_amt);
				order.price = quote.quanto_ask;
			}else {
				order.amount = quote.bid_tgt * Math.abs(trd_amt);	
				order.price = quote.quanto_bid;
			}
		}else {
			order.dir = oppDir;
			if(oppDir == OrderDirection.sell) {
				order.amount = quote.ask_tgt * Math.abs(trd_amt) * quote.quanto_ask;
				order.price = 1 / quote.quanto_ask;
			}else {
				order.amount = quote.bid_tgt * Math.abs(trd_amt) * quote.quanto_bid;	
				order.price = 1 / quote.quanto_bid;
			}						
		}
		
		order.marketType = MarketType.market;
		order.userref = orderId++;
		order.orderState = OrderState.Adding;
		order.exchangeConnector = quote.quanto_exchangeConnector;
		order.parent = quote;
		
		return order;
	}	
	
	private void AdjustTradingBalance(Order order, iBalance securBalance, iBalance moneyBalance) {
		if(order.dir == OrderDirection.buy) {
			// 1.0045 = 1 +
			// 0.2% - накидка за то, что сделка выше, чем заплинировали
			// +
			// 0.25% - комиссия биржи
			
			securBalance.setBalanceValue(securBalance.Availlable() + order.amount);
			moneyBalance.setBalanceValue(moneyBalance.Availlable() - order.amount * order.price * 1.0045);
		}else {
			// 0.9955 = 1 -
			// 0.2% - накидка за то, что сделка ниже, чем заплинировали
			// -
			// 0.25% - комиссия биржи

			securBalance.setBalanceValue(securBalance.Availlable() - order.amount);
			moneyBalance.setBalanceValue(moneyBalance.Availlable() + order.amount * order.price * 0.9955);
		}
	}
	
	

	private boolean CheckTradingBalance(boolean portfolio_buy, Order order, iBalance securBalance, iBalance moneyBalance) {
		Quote quote = (Quote) order.parent;
		
		if(this.usedConnections.get(quote.connection_id) > 0) return false;		
		
		Portfolio portfolio = quote.portfolio;
		
		if(order.dir == OrderDirection.buy) {
			// 1.0045 = 1 +
			// 0.2% - накидка за то, что сделка выше, чем заплинировали
			// +
			// 0.25% - комиссия биржи

			iBalance balance = portfolio.neededBalances.get(moneyBalance.BalanceId());
			portfolio.neededBalances.get(securBalance.BalanceId()).setBalanceValue(0);
			
			double adjustedMoneyBalance = moneyBalance.Availlable() - order.amount * order.price * 1.0045;
			if(adjustedMoneyBalance > 0) {				
				balance.setBalanceValue(0);
				
				return true;
			}
			else {
				int potential;
				int limit;
				
				
				if(portfolio_buy) {
					potential = (int) ((portfolio.buy_at - portfolio.wask) / portfolio.shift) + 1;					
					limit =  (int) ((portfolio.buy_limit - portfolio.amount) / portfolio.trd_amt) + 1;
				}else {
					potential = (int) ((portfolio.wbid - portfolio.sell_at) / portfolio.shift) + 1;					
					limit =  (int) ((portfolio.amount - portfolio.sell_limit) / portfolio.trd_amt) + 1;					
				}
				
				potential = Math.min(Math.max(0, potential), Math.max(0, limit));
				
				if(potential == 0) 
					potential = 1;				
				
				double neededBalance = order.amount * order.price * 1.0045 * potential - moneyBalance.Availlable();
				
				
				balance.setBalanceValue(neededBalance);
				
				return false;
			}
		}else {
			iBalance balance = portfolio.neededBalances.get(securBalance.BalanceId());
			portfolio.neededBalances.get(moneyBalance.BalanceId()).setBalanceValue(0);
			
			double adjustedSecurBalance = securBalance.Availlable() - order.amount;
			if(adjustedSecurBalance > 0) { 
				balance.setBalanceValue(0);
				
				
				return true;			
			}
			else {
				
				int potential;
				int limit;
				
				if(portfolio_buy) {
					potential = (int) ((portfolio.buy_at - portfolio.wask) / portfolio.shift) + 1;					
					limit =  (int) ((portfolio.buy_limit - portfolio.amount) / portfolio.trd_amt) + 1;
				}else {
					potential = (int) ((portfolio.wbid - portfolio.sell_at) / portfolio.shift) + 1;					
					limit =  (int) ((portfolio.amount - portfolio.sell_limit) / portfolio.trd_amt) + 1;					
				}

				potential = Math.min(Math.max(0, potential), Math.max(0, limit));

				if(potential == 0) 
					potential = 1;
				
				double neededBalance = order.amount * potential - securBalance.Availlable();
				
				
				balance.setBalanceValue(neededBalance);
				
				return false;
			}
			
		}
	}
	
	
	
	private void QuantoQuoteChanged(Quote quote, MarketData marketData) {
		Portfolio portfolio = quote.portfolio;
		
		int bids_num = 0;
		int asks_num = 0;
		
		double q_bid_ch;
		double q_ask_ch;
		if(quote.is_quanto_mltp == 0) {
			if(marketData.getLowestAsk() == 0) {
				q_bid_ch = 0 - quote.quanto_bid;
				if(quote.quanto_bid != 0) bids_num++;
			}
			else {
				q_bid_ch = 1 / marketData.getLowestAsk() - quote.quanto_bid;
				if(quote.quanto_bid == 0) bids_num--;
			}
			
			if(marketData.getHighestBid() == 0) {
				q_ask_ch = 0 - quote.quanto_ask;
				if(quote.quanto_ask != 0) asks_num++;
			}
			else {
				q_ask_ch = 1 / marketData.getHighestBid() - quote.quanto_ask;
				if(quote.quanto_ask == 0) asks_num--;				
			}
		}else {
			q_bid_ch = marketData.getHighestBid() - quote.quanto_bid;
			q_ask_ch = marketData.getLowestAsk() - quote.quanto_ask;
			
			if(marketData.getHighestBid() == 0 && quote.quanto_bid != 0) bids_num++;
			else if(marketData.getHighestBid() != 0 && quote.quanto_bid == 0) bids_num--;
			
			if(marketData.getLowestAsk() == 0 && quote.quanto_ask != 0) asks_num++;
			else if(marketData.getLowestAsk() != 0 && quote.quanto_ask == 0) asks_num--;			
		}
		
		quote.quanto_bid += q_bid_ch;
		quote.quanto_ask += q_ask_ch;
		
		
		
		double w_bid_ch;
		double w_ask_ch;
		
		if(quote.weight > 0) {
			w_bid_ch = q_bid_ch * quote.weight * quote.bid_tgt;
			w_ask_ch = q_ask_ch * quote.weight * quote.ask_tgt;
			
			portfolio.zero_wbids_num += bids_num;
			portfolio.zero_wasks_num += asks_num;
		}else {
			w_ask_ch = q_bid_ch * quote.weight * quote.bid_tgt;
			w_bid_ch = q_ask_ch * quote.weight * quote.ask_tgt;			

			portfolio.zero_wbids_num += asks_num;
			portfolio.zero_wasks_num += bids_num;
		}
		
		portfolio.wbid += w_bid_ch;
		portfolio.wask += w_ask_ch;
	}
	
	
	private void QuoteChanged(Quote quote, MarketData marketData) {
		Portfolio portfolio = quote.portfolio;

		
		double bid_ch;
		double ask_ch;
		
		bid_ch = marketData.getHighestBid() - quote.bid_tgt;
		ask_ch = marketData.getLowestAsk() - quote.ask_tgt;

		
		int bids_num = 0;
		int asks_num = 0;

		if(marketData.getHighestBid() == 0 && quote.bid_tgt != 0) bids_num++;
		else if(marketData.getHighestBid() != 0 && quote.bid_tgt == 0) bids_num--;
		
		if(marketData.getLowestAsk() == 0 && quote.ask_tgt != 0) asks_num++;
		else if(marketData.getLowestAsk() != 0 && quote.ask_tgt == 0) asks_num--;			

		quote.bid_tgt = marketData.getHighestBid();
		quote.ask_tgt = marketData.getLowestAsk() ;

		
		double w_bid_ch;
		double w_ask_ch;
		
		if(quote.weight > 0) {
			w_bid_ch = bid_ch * quote.weight * quote.quanto_bid;
			w_ask_ch = ask_ch * quote.weight * quote.quanto_ask;
			
			portfolio.zero_wbids_num += bids_num;
			portfolio.zero_wasks_num += asks_num;			
		}else {
			w_bid_ch = ask_ch * quote.weight * quote.quanto_ask;
			w_ask_ch = bid_ch * quote.weight * quote.quanto_bid;
			
			portfolio.zero_wbids_num += asks_num;
			portfolio.zero_wasks_num += bids_num;
		}
		quote.wbid += w_bid_ch;
		quote.wask += w_ask_ch;
		
		portfolio.wbid += w_bid_ch;
		portfolio.wask += w_ask_ch;
	}
	

	
	


	@Override
	public void ConnectionStateChanged(ConnectionState connectionState) {
		// TODO Auto-generated method stub
		
		//synchronized(quoteConnectors) {
		synchronized (this) {
			ArrayList<Quote> qList = quoteConnectors.get(connectionState.connectionId);
			if(qList == null) return;
					
			for(Quote quote : qList) {
				Portfolio portfolio = quote.portfolio;
				
				if(connectionState.connectionState == StateEnum.Connected)
					portfolio.dead_connectors--;
				else
					portfolio.dead_connectors++;
			}
		}
	}



	@Override
	public void OrderStateChanged(Order orderState) {
		// TODO Auto-generated method stub
		
		
		
		//synchronized (activeOrdersMap) {
		synchronized (this) {
			Order myOrder = this.activeOrdersMap.get(orderState.userref);
			if(myOrder == null) return;
			
			Quote quote = (Quote) orderState.parent;
			Portfolio portfolio = quote.portfolio;
			
			
			this.usedConnections.put(quote.connection_id, this.usedConnections.get(quote.connection_id) - 1);

			
			switch(orderState.orderState) {
			case Active:{
				myOrder.orderState = OrderState.Moving;
				
				if(orderState.amount == 0) {
					portfolio.active_orders--;
					this.activeOrdersMap.remove(orderState.userref);
				}else {
					orderState.orderState = OrderState.Killing;
					orderState.exchangeConnector.KillOrder(orderState);
				}
				
			}break;
			case Killed:{
				myOrder.orderState = OrderState.Moving;

				if(orderState.amount == 0) {
					portfolio.active_orders--;
					this.activeOrdersMap.remove(orderState.userref);
				}else if(myOrder.orderState == OrderState.Moving){
					
					if(orderState.dir == OrderDirection.buy)orderState.price = quote.ask_tgt;
					else orderState.price = quote.bid_tgt;
					
					orderState.orderState = OrderState.Adding;
					orderState.exchangeConnector.AddOrder(orderState);
				}	
				
			}break;
			case Executed:{
				portfolio.active_orders--;
				this.activeOrdersMap.remove(orderState.userref);
				
			}break;
			case Refused:{
				portfolio.trade = 0;
				portfolio.active_orders--;
				this.activeOrdersMap.remove(orderState.userref);
				
			}break;
			default:
				break;
			}

	
		}
		
	}

	private void PortfolioDataChanged(PortfolioTrade portfolioTrade) {
		synchronized (this.portfoliosToSaveSet) {
			this.portfoliosToSaveSet.add(portfolioTrade);
		}
	}
	
	@Override
	public void BalanceStateChanged(iBalance balance) {
		// TODO Auto-generated method stub
		
		synchronized (this.balancesToSaveList) {
			this.balancesToSaveList.add(balance);
			
			this.unsycBalancesMap.put(balance.Exchange() + balance.CurrencyCode(), balance);
		}
		
	//	System.out.println(balance.toString());
	}

	@Override
	public void TradeExecuted(iTrade trade) {
		// TODO Auto-generated method stub
		
		synchronized (this.tradesToSaveList) {
			this.tradesToSaveList.add(trade);
		}

		
	//	System.out.println(trade.toString());
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while(true){
			//long mills = Math.abs(rnd.nextLong()) % 100;
			long mills = 1000;
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			CheckSystemState();
			SaveDataToDB();
			
		}
	}
	
	void SaveDataToDB() {
		
		synchronized(this.balancesToSaveList) {
			ArrayList<iBalance> tmpArray;
			if(this.balancesToSaveList.size() > 0){
				tmpArray = this.balancesToSaveList;
				this.balancesToSaveList = this.balancesToSaveListSwap;
				this.balancesToSaveListSwap = tmpArray;
			}
		}
		
		for(iBalance balance : this.balancesToSaveListSwap)
			SaveBalanceToDB(balance);
		
		this.balancesToSaveListSwap.clear();
		
		
		synchronized(this.tradesToSaveList) {
			ArrayList<iTrade> tmpArray;
			if(this.tradesToSaveList.size() > 0){
				tmpArray = this.tradesToSaveList;
				this.tradesToSaveList = this.tradesToSaveListSwap;
				this.tradesToSaveListSwap = tmpArray;
			}
		}
		
		for(iTrade trade : this.tradesToSaveListSwap)
			SaveTradeToDB(trade);
		
		this.tradesToSaveListSwap.clear();
		
		
		synchronized(this.portfoliosToSaveSet) {
			HashSet<PortfolioTrade> tmpArray;
			if(this.portfoliosToSaveSet.size() > 0){
				tmpArray = this.portfoliosToSaveSet;
				this.portfoliosToSaveSet = this.portfoliosToSaveSetSwap;
				this.portfoliosToSaveSetSwap = tmpArray;
			}
		}
		
		for(PortfolioTrade portfolioTrade : this.portfoliosToSaveSetSwap)
			UpdatePortfolioData(portfolioTrade);
		
		
		this.portfoliosToSaveSetSwap.clear();
		
		SaveRequestedBalances();
		
		
		synchronized(this.exceptionsToSaveList) {
			ArrayList<String> tmpArray;
			if(this.exceptionsToSaveList.size() > 0){
				tmpArray = this.exceptionsToSaveList;
				this.exceptionsToSaveList = this.exceptionsToSaveListSwap;
				this.exceptionsToSaveListSwap = tmpArray;
			}
		}
		
		for(String excpetionString : this.exceptionsToSaveListSwap)
			SaveException(excpetionString);
		
		
		this.exceptionsToSaveListSwap.clear();
		
	}
	
	
	void SaveException(String exceptionString) {
		try {
			newExceptionStatement.setString(1, exceptionString);
			newExceptionStatement.setLong(2, System.currentTimeMillis());
			
			newExceptionStatement.execute();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void SaveRequestedBalances() {
		long now = System.currentTimeMillis();
		
		if(now - lastUpdatedRequest >= 60000) {
			lastUpdatedRequest = now;
			
			
			
			for(Portfolio portfolio : this.portfolios.values())
				for(iBalance balance : portfolio.neededBalances.values())
					if(balance.BalanceValue() != 0)
						SaveRequestedBalance(portfolio.portfolio_id, balance, lastUpdatedRequest);
		}
	}
	
	
	void SaveRequestedBalance(int portfolio_id, iBalance balance, Long momentUTC) {
		try {
			reqBalancePreparedStatement.setInt(1, portfolio_id);
			reqBalancePreparedStatement.setInt(2, balance.BalanceId());
			reqBalancePreparedStatement.setDouble(3, balance.BalanceValue());
			reqBalancePreparedStatement.setLong(4, momentUTC);
			
			reqBalancePreparedStatement.execute();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void UpdatePortfolioData(PortfolioTrade portfolioTrade) {
		Portfolio portfolio = this.portfolios.get(portfolioTrade.portfolio_id);
		
		try {
			updatePortfoliosPreparedStatement.setDouble(1, portfolio.amount);
			updatePortfoliosPreparedStatement.setDouble(2, portfolio.buy_at);
			updatePortfoliosPreparedStatement.setDouble(3, portfolio.sell_at);
			updatePortfoliosPreparedStatement.setDouble(4, portfolio.portfolio_id);			
			updatePortfoliosPreparedStatement.execute();	
			
			
			newPortfolioTradeStatement.setInt(1, portfolioTrade.portfolio_id);
			newPortfolioTradeStatement.setLong(2, portfolioTrade.portfolio_trade_id);
			newPortfolioTradeStatement.setDouble(3, portfolioTrade.trd_amt);
			newPortfolioTradeStatement.setDouble(4, portfolioTrade.price);
			newPortfolioTradeStatement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void SaveBalanceToDB(iBalance balance) {
		
				
		try {
			balancePreparedStatement.setString (1, balance.Exchange());
			balancePreparedStatement.setString (2, balance.CurrencyCode());
			balancePreparedStatement.setDouble (3, balance.BalanceValue());
			balancePreparedStatement.setDouble (4, balance.Availlable());
			balancePreparedStatement.setLong(5, System.currentTimeMillis());
			
			//System.out.println(preparedStmt.toString());
			
			balancePreparedStatement.execute();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	void SaveTradeToDB(iTrade trade) {
		
		
		try {
			newTradePreparedStatement.setString (1, trade.getExchange());
			newTradePreparedStatement.setString (2, trade.getTrdPair());
			newTradePreparedStatement.setString (3, trade.getTradeId());
			newTradePreparedStatement.setLong (4, trade.getMomentLinuxUTC());
			newTradePreparedStatement.setDouble (5, trade.getPrice());
			newTradePreparedStatement.setDouble (6, trade.getAmount());
			newTradePreparedStatement.setDouble (7, trade.getCommiss());
			newTradePreparedStatement.setString (8, trade.getDir());
			newTradePreparedStatement.setString(9, trade.getOrderAcceptanceId());
			newTradePreparedStatement.setLong(10, trade.getPortfolioTradeId());
			newTradePreparedStatement.setLong(11, trade.getUserRefId());
			
			//System.out.println(preparedStmt.toString());
			
			newTradePreparedStatement.execute();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void ExceptionIsAraised(String exceptionString) {
		// TODO Auto-generated method stub
		
		synchronized (this.exceptionsToSaveList) {
			this.exceptionsToSaveList.add(exceptionString);
		}
	}
}
