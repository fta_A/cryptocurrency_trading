package pairtrading;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import interfaces.iBalance;

public class Portfolio {
	
	
	
	public int portfolio_id;

	public ArrayList<Quote> quotes_tbl = new ArrayList<Quote>();

	public double wbid;
	public double wask;

	public double buy_at;
	public double sell_at;

	public double trd_amt;
	public double buy_limit;
	public double sell_limit;
	public double shift;

	
	public int active_orders;
	public double amount;

	public int work_type;
	public int state;

	public int zero_wbids_num = 0;
	public int zero_wasks_num = 0;
	
	public int dead_connectors = 0;
	public int used_connectors = 0;

	public int spread;
	public int mkt_spread;

	public int trade;
	public int buy;
	public int sell;
	
	public HashMap<Integer, iBalance> neededBalances = new HashMap<Integer, iBalance>();

	DecimalFormat decimalFormat;
	
	public Portfolio() {
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);

		decimalFormat = (DecimalFormat)nf;
		decimalFormat.setMaximumFractionDigits(10);	
		decimalFormat.setGroupingUsed(false);
	}
	
	@Override
	public String toString() {
		return "Portfolio [portfolio_id=" + portfolio_id 
				+ ", trade=" + trade 
				+ ", trd_amt=" + decimalFormat.format(trd_amt) 
				+ ", amount=" + amount 
				+ ", dead_conn=" + dead_connectors 
//				+ ", work_type=" + work_type 
//				+ ", state=" + state 
				+ ", buy=" + buy 
				+ ", wask=" + decimalFormat.format(wask)
				+ ", buy_at=" + decimalFormat.format(buy_at)
				+ ", buy_limit=" + buy_limit 
				+ ", zero_wasks_num=" + zero_wasks_num 

				+ ", sell=" + sell 
				+ ", wbid=" + decimalFormat.format(wbid)
				+ ", sell_at=" + decimalFormat.format(sell_at)
				+ ", sell_limit=" + sell_limit 
				+ ", zero_wbids_num=" + zero_wbids_num 
				
				+ ", shift=" + shift 
				+ ", active_orders=" + active_orders
				+ ", spread=" + spread 
				+ "]";
	}
	
	
}
