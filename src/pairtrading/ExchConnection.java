package pairtrading;

public class ExchConnection {
	int connection_id;
	int exchange_code_id;
	String class_id;
	String connection_string;
	String exchange_code;
	String api_key;
	String private_key;
	public int getConnection_id() {
		return connection_id;
	}
	public void setConnection_id(int connection_id) {
		this.connection_id = connection_id;
	}
	public String getClass_id() {
		return class_id;
	}
	public void setClass_id(String class_id) {
		this.class_id = class_id;
	}
	public String getConnection_string() {
		return connection_string;
	}
	public void setConnection_string(String connection_string) {
		this.connection_string = connection_string;
	}
	
	public String getApi_key() {
		return api_key;
	}
	public void setApi_key(String api_key) {
		this.api_key = api_key;
	}
	public String getPrivate_key() {
		return private_key;
	}
	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}
	public int getExchange_code_id() {
		return exchange_code_id;
	}
	public void setExchange_code_id(int exchange_code_id) {
		this.exchange_code_id = exchange_code_id;
	}
	public String getExchange_code() {
		return exchange_code;
	}
	public void setExchange_code(String exchange_code) {
		this.exchange_code = exchange_code;
	}
	
}
