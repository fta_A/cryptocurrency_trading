package bitflyer.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.MarketType;
import connector.Order.OrderState;
import connector.OrderStateListener;

public class BitflyerOrderStateListener1  extends OrderStateListener {

	ObjectMapper mapper = new ObjectMapper();
	
	String URLString = "https://api.bitflyer.com";
	String addOrderPathString = "/v1/me/sendchildorder";	
	String cancelOrderPathString = "/v1/me/cancelchildorder";	
	String openedOrdersPathString = "/v1/me/getchildorders";
	
	String closedOrdersPathString = "/0/private/ClosedOrders";
	
	int requestOrder = 0;
	
	HashMap<String, Order> activeOrdersMap;
	HashMap<String, Order> killedOrdersMap;
	
	ArrayList<Order> updatedOrders;
	ArrayList<Order> errorOrders;
	HashMap<Integer, Order> lostOrders;
	
	
	ArrayList<Order> newOrders;
	
	HashMap<String, Long> exchangeOrderIds;
	ArrayList<String> availableProducts;
	int availableProductsId = 0;

	
	public BitflyerOrderStateListener1(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		
		// TODO Auto-generated constructor stub
		this.activeOrdersMap = new HashMap<String, Order>();
		this.killedOrdersMap = new HashMap<String, Order>();
		
		this.updatedOrders = new ArrayList<Order>();
		this.errorOrders = new ArrayList<Order>();
		this.newOrders = new ArrayList<Order>();
		this.lostOrders = new HashMap<Integer, Order>();
		
		
		this.exchangeOrderIds = new HashMap<String, Long>();
		
		this.availableProducts = new ArrayList<>();
		this.availableProducts.add("BTC_JPY");
		this.availableProducts.add("ETH_BTC");
	}

	
	/*
	public BitflyerOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			int connectionId, String apiKey, String privateKey) {
		super(iOrderStateEvent, floodControl, connectionId);
		// TODO Auto-generated constructor stub
		
		this.activeOrdersMap = new HashMap<String, Order>();
		this.killedOrdersMap = new HashMap<String, Order>();
		
		this.updatedOrders = new ArrayList<Order>();
		this.errorOrders = new ArrayList<Order>();
		this.newOrders = new ArrayList<Order>();
		this.lostOrders = new HashMap<Integer, Order>();
		
		
		this.exchangeOrderIds = new HashMap<String, Long>();
		
		this.availableProducts = new ArrayList<>();
		this.availableProducts.add("BTC_JPY");
		this.availableProducts.add("ETH_BTC");
	}
*/
	
	
	
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		
		InitChildOrders();
		
		while(true){
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			ArrayList<Order> tmpArrayList_Order;
			synchronized(this.ordersToSend) {
				if(this.ordersToSend.size() > 0){
					tmpArrayList_Order = this.ordersToSend;
					this.ordersToSend = this.newOrders;
					this.newOrders = tmpArrayList_Order;
				}
			}
			
			for(Order order: this.newOrders) {
				switch(order.orderType) {
				case NewOrder:
					AddOrder(order);
				break;
				case KillOrder:
					Order activeOrder = this.activeOrdersMap.get(order.transIdList.get(0));
					if(activeOrder != null) KillOrder(activeOrder);
				break;
				default:
					break;
				}
			}			
			this.newOrders.clear();
			
			if(this.activeOrdersMap.size() > 0 || this.killedOrdersMap.size() > 0) {
				synchronized (this.floodControl) {
					int availableRequests = this.floodControl.AvailableRequests();
					
					if(availableRequests > 0){
						if(requestOrder == 0) {
							if(this.activeOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}else if(requestOrder == 1) {
							if(this.killedOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}


						switch(requestOrder) {
						case 0:
							CheckOpenedOrders();
							break;
						case 1:
							CheckClosedOrders();
							break;
						}
						
						
						requestOrder = (requestOrder + 1) % 2;					
						this.floodControl.NewRequest();
					}
				}
			}
			
			for(Order order : this.updatedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.updatedOrders.clear();
			

		}
	}
	
	private void InitChildOrders() {
		String method = "GET";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		for(String trdPair : availableProducts) {
			String path = this.openedOrdersPathString + "?product_code=" + trdPair;
			String body = "";
			
			JsonNode jn = getPrivateAnswer(path, body, method, timestamp);
			
	        TypeReference<ArrayList<BitflyerChildOrdersReply>> listTypeReference = new TypeReference<ArrayList<BitflyerChildOrdersReply>> () {};
	        ArrayList<BitflyerChildOrdersReply> list = mapper.convertValue(jn, listTypeReference);
	
	        if(list == null) return;
	        
	        Long maxValue = (long) 0;
	        for(BitflyerChildOrdersReply order : list) {
	        	if(order.getId() > maxValue) maxValue = order.getId();
	        }
	        
	        this.exchangeOrderIds.put(trdPair, maxValue);
		}
	}
	
	private void CheckOpenedOrders() {
		String trdPair = this.availableProducts.get(availableProductsId);
		availableProductsId = (availableProductsId + 1) % this.availableProducts.size();

		String path = this.openedOrdersPathString;
		String method = "GET";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		path += "?product_code=" + trdPair;
		path += "&after=" + this.exchangeOrderIds.get(trdPair);

		String body = "";
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp);
		
        TypeReference<ArrayList<BitflyerChildOrdersReply>> listTypeReference = new TypeReference<ArrayList<BitflyerChildOrdersReply>> () {};
        ArrayList<BitflyerChildOrdersReply> list = mapper.convertValue(jn, listTypeReference);

       
        for(BitflyerChildOrdersReply ticker : list) {
			
			Order order = this.activeOrdersMap.get(ticker.getChild_order_acceptance_id());
			
			if(order != null) {
				if(order.orderState == OrderState.Adding) {
					order.orderState = OrderState.Active;
					
					if(order.transIdList == null) order.transIdList = new ArrayList<String>();
					order.transIdList.add(ticker.getChild_order_acceptance_id());
					
					order.amount = 0; 
				}
				
				double residual = ticker.getOutstanding_size();		    			
    			if(order.amount != residual) {
    				order.amount = residual;
    				this.iOrderStateEvent.OrderStateChanged(order);	
    			}
				
    			
    			
			}
        	
        }
	}
	
	private void CheckClosedOrders() {
		

	}
	
	
	private void KillOrder(Order order) {
		String path = this.addOrderPathString;
		String method = "POST";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String body = "{"
				+ "\"product_code\" : \"" + order.trdPair + "\", "
				+ "\"child_order_acceptance_id\" : \"" + order.transIdList.get(0) + "\""
				+ "}";
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp);
		
		System.out.println("BTFL KILL: " + order.toString() + ". Answer: " + jn);
		
		if(jn != null) {
			this.activeOrdersMap.remove(order.transIdList.get(0));
        	
        	order.orderState = OrderState.Killed;
        	
        	for(String txid: order.transIdList) {
        		this.killedOrdersMap.put(txid, order);
        	}
		}else {
			NewOrder(order);
		}
		
	}
	
	private void AddOrder(Order order) {
		String path = this.addOrderPathString;
		String method = "POST";
		String timestamp = String.valueOf(System.currentTimeMillis());
		
		String body = "{"
				+ "\"product_code\" : \"" + order.trdPair + "\", "
				+ "\"child_order_type\" : \"" + order.marketType.toString().toUpperCase() + "\", "
				+ "\"side\" : \"" + order.dir.toString().toUpperCase() + "\", "
				+ "\"price\" : " + order.price + ", "
				+ "\"size\" : " + order.amount 
				+ "}";
		
		JsonNode jn = getPrivateAnswer(path, body, method, timestamp);

		System.out.println("BTFL ADD: " + order.toString() + ". Answer: " + jn);
		
		if(jn != null) {
			this.connectionStateListener.OrderStateConnected();
			
	        BitflyerAddOrderResult addOrderResult = mapper.convertValue(jn, BitflyerAddOrderResult.class);
	    	order.transIdList.clear();
	    	order.transIdList.add(addOrderResult.getChild_order_acceptance_id());
		
	    	
	    	if(order.marketType == MarketType.market) {
	    		order.orderState = OrderState.Executed;
	    		order.amount = 0;
	    		/*
	        	for(String txid: order.transIdList) {
	        		this.killedOrdersMap.put(txid, order);
	        	}
	        	*/
	    		this.updatedOrders.add(order);
	    	}else {
	    		order.orderState = OrderState.Active;
	    		this.activeOrdersMap.put(addOrderResult.getChild_order_acceptance_id(), order);	 
	    		this.updatedOrders.add(order);
	    	}
	    	
	    	
		}else {
			//this.errorOrders.add(order);
			//this.activeOrdersMap.put(order.userref, order);
			this.connectionStateListener.OrderStateDisconnected();
			NewOrder(order);
		}
		
	}
	
	private JsonNode getPrivateAnswer(String path, String body, String method, String timestamp ) {
		JsonNode jsonReply = null;
		
		if(privateKey == null) return null;
		
		String text = timestamp + method + path + body;
		String sign = calculateSignature(privateKey, "HmacSHA256", text);
		
		try {
			URL postURL = new URL(this.URLString + path);
			
			HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
			httpsURLConnection.setRequestMethod(method);
		
			httpsURLConnection.setRequestProperty("ACCESS-KEY", apiKey);
			httpsURLConnection.setRequestProperty("ACCESS-TIMESTAMP", timestamp);
			httpsURLConnection.setRequestProperty("ACCESS-SIGN", sign);
			httpsURLConnection.setRequestProperty("Content-Type", "application/json");
			httpsURLConnection.setDoOutput(true);
			
			if(body.length() > 0) {
				OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
		        writer.write(body);
		        writer.flush();
			}
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();
	        
	        jsonReply = mapper.readTree(jsonString);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
		
		return jsonReply;
	}
	
	protected String calculateSignature(String privateKey, String shaInstance, String postData) {
	    String signature = "";
	    try {
	        Mac hmacSHA256 = Mac.getInstance(shaInstance);
	        SecretKeySpec secret_key = new SecretKeySpec(privateKey.getBytes(), shaInstance);
	        hmacSHA256.init(secret_key);
	        signature = Hex.encodeHexString(hmacSHA256.doFinal(postData.getBytes()));

	    } catch(Exception e) {
	    	
	    }
	    
	    return signature;		
	}
	
	
}
