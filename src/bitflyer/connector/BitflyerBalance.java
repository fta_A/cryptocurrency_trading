package bitflyer.connector;

import interfaces.iBalance;

public class BitflyerBalance implements iBalance{
	
	String exchange;	
	
	public String currency_code;
	public double amount;
	public double available;

	
	@Override
	public void setCurrencyCode(String currencyCode) {
		this.currency_code = currencyCode;
	}

	@Override
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	@Override
	public void setBalanceValue(double balanceValue) {
		// TODO Auto-generated method stub
		this.amount = balanceValue;
	}

	
	@Override
	public String Exchange() {
		// TODO Auto-generated method stub
		return this.exchange;
	}

	@Override
	public String CurrencyCode() {
		// TODO Auto-generated method stub
		return this.currency_code;
	}

	@Override
	public double BalanceValue() {
		// TODO Auto-generated method stub
		return this.amount;
	}

	@Override
	public double Availlable() {
		// TODO Auto-generated method stub
		return this.available;
	}

	@Override
	public double Pending() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String Address() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int BalanceId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	@Override
	public String toString() {
		return "Balance "
				+ "["
				+ "exchnage=" + this.Exchange()
				+ ", currency_code=" + this.CurrencyCode() 
				+ ", amount=" + this.BalanceValue()
				+ ", available=" + this.Availlable() 
				+ "]";
	}



	
}
