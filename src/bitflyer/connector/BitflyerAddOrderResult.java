package bitflyer.connector;

import connector.AddOrderResult;

public class BitflyerAddOrderResult extends AddOrderResult{
	String child_order_acceptance_id;

	public String getChild_order_acceptance_id() {
		return child_order_acceptance_id;
	}

	public void setChild_order_acceptance_id(String child_order_acceptance_id) {
		this.child_order_acceptance_id = child_order_acceptance_id;
		this.orderNumber = child_order_acceptance_id;
	}
	
	
}
