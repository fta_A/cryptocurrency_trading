package bitflyer.connector;

public class BitflyerChildOrdersReply {
	private Long id;
	private String child_order_id;
	private String product_code;
	private String side;
	private String child_order_type;
	private double price;
	private double average_price;
	private double size;
	private String child_order_state;
	private String expire_date;
	private String child_order_date;
	private String child_order_acceptance_id;
	private double outstanding_size;
	private double cancel_size;
	private double executed_size;
	private double total_commission;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getChild_order_id() {
		return child_order_id;
	}
	public void setChild_order_id(String child_order_id) {
		this.child_order_id = child_order_id;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public String getChild_order_type() {
		return child_order_type;
	}
	public void setChild_order_type(String child_order_type) {
		this.child_order_type = child_order_type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAverage_price() {
		return average_price;
	}
	public void setAverage_price(double average_price) {
		this.average_price = average_price;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getChild_order_state() {
		return child_order_state;
	}
	public void setChild_order_state(String child_order_state) {
		this.child_order_state = child_order_state;
	}
	public String getExpire_date() {
		return expire_date;
	}
	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}
	public String getChild_order_date() {
		return child_order_date;
	}
	public void setChild_order_date(String child_order_date) {
		this.child_order_date = child_order_date;
	}
	public String getChild_order_acceptance_id() {
		return child_order_acceptance_id;
	}
	public void setChild_order_acceptance_id(String child_order_acceptance_id) {
		this.child_order_acceptance_id = child_order_acceptance_id;
	}
	public double getOutstanding_size() {
		return outstanding_size;
	}
	public void setOutstanding_size(double outstanding_size) {
		this.outstanding_size = outstanding_size;
	}
	public double getCancel_size() {
		return cancel_size;
	}
	public void setCancel_size(double cancel_size) {
		this.cancel_size = cancel_size;
	}
	public double getExecuted_size() {
		return executed_size;
	}
	public void setExecuted_size(double executed_size) {
		this.executed_size = executed_size;
	}
	public double getTotal_commission() {
		return total_commission;
	}
	public void setTotal_commission(double total_commission) {
		this.total_commission = total_commission;
	}
	
}
