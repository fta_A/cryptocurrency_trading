package bitflyer.connector;

import java.util.ArrayList;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;


public class BitflyerMarketDataListener extends MarketDataListener {
	
	public BitflyerMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
	
		/*
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("BTC_JPY", "https://api.bitflyer.com/v1/getticker?product_code=BTC_JPY");
		urlsMap.put("ETH_BTC", "https://api.bitflyer.com/v1/getticker?product_code=ETH_BTC");
		
		
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructType(BitflyerTicker.class)
			, ""
		);
		*/
		
		this.SetTickersListSource(TickersListType.OneToOne
				, ""
				, "https://api.bitflyer.com/v1/getmarkets"
				//, mapper.getTypeFactory().constructMapType(Map.class, String.class, BitflyerAssetPair.class)
				, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BitflyerAssetPair.class)
				, ""
				, "https://api.bitflyer.com/v1/getticker?product_code="
				, ""
				, mapper.getTypeFactory().constructType(BitflyerTicker.class)
				, ""
			);
	}
	
}

