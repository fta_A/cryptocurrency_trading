package bitflyer.connector;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import connector.Trade;

public class BitflyerTrade extends Trade{
	
	Long id;
	String side;
	double size;
	String exec_date;
	String child_order_id;
	double commission;
	String child_order_acceptance_id;
	

	public String getExec_date() {
		return exec_date;
	}

	public void setExec_date(String exec_date) {
		exec_date = exec_date.toUpperCase();
		this.exec_date = exec_date;
		
		DateFormat dateFormat;
		
		if(this.exec_date.length() == 19)
			dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		else
			dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date dt;
		
		try {
			dt = (Date) dateFormat.parse(this.exec_date);
			this.momentLinuxUTC = dt.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
		this.tradeId = id.toString();
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
		this.dir = side;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
		this.amount = size;
	}

	public String getChild_order_id() {
		return child_order_id;
	}

	public void setChild_order_id(String child_order_id) {
		this.child_order_id = child_order_id;
		this.orderAcceptanceId = child_order_id.toString();
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
		this.commiss = commission;
	}

	public String getChild_order_acceptance_id() {
		return child_order_acceptance_id;
	}

	public void setChild_order_acceptance_id(String child_order_acceptance_id) {
		this.child_order_acceptance_id = child_order_acceptance_id;
	}

	

}
