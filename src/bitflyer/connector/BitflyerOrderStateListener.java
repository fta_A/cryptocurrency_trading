package bitflyer.connector;


import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.MarketType;
import connector.OrderStateListener;
import interfaces.iTrade;

public class BitflyerOrderStateListener  extends OrderStateListener {

	public BitflyerOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls("https://api.bitflyer.com"
				, "/v1/me/sendchildorder"
				, "/v1/me/cancelchildorder"
				, "GET /v1/me/getbalance"
				, "GET /v1/me/getexecutions"
				);
		
		this.SetSignMethod("HmacSHA256");
		
		this.SetTickerAndBalanceTypes(
				mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BitflyerTrade.class)
				, null
				
				, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, BitflyerBalance.class)
				, null
				
				// For Closed Orders
				, null
				, null
				
				// For Add order Results
				, mapper.constructType(BitflyerAddOrderResult.class)
				, null
				
				);
		
	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		if(order.marketType == MarketType.market) order.price = 0;
		
		String body = "{"
				+ "\"product_code\" : \"" + order.trdPair + "\", "
				+ "\"child_order_type\" : \"" + order.marketType.toString().toUpperCase() + "\", "
				+ "\"side\" : \"" + order.dir.toString().toUpperCase() + "\", "
				+ "\"price\" : " + decimalFormat.format(order.price) + ", "
				+ "\"size\" : " + decimalFormat.format(order.amount) 
				+ "}";

		return body;
	}
	
	@Override
	protected String GetTradesPath(String path, String nonce, String trdPair, String from) {
		return path + "?product_code=" + trdPair 
				+ "&after=" + from;
	}
	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("ACCESS-KEY", apiKey);
		httpsURLConnection.setRequestProperty("ACCESS-TIMESTAMP", timestamp);
		httpsURLConnection.setRequestProperty("ACCESS-SIGN", signature);
		httpsURLConnection.setRequestProperty("Content-Type", "application/json");
	}
	
	@Override
	protected String GetStringToSign(String timestamp, String method, String path, String body) {
		return timestamp + method + path + body;
	}
	
	@Override
	protected String GetFromForGetTradeRequest(iTrade trade) {
		return trade.getTradeId();
	}

}
