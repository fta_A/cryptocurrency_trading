package bitflyer.connector;


import connector.ExchangeConnector;

public class BitflyerConnector extends ExchangeConnector {
	public BitflyerConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
		
		this.connectionStateListener = new BitflyerConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new BitflyerMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);
		
		this.orderStateListener = new BitflyerOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);

	}

}
