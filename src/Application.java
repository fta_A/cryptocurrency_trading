
import java.io.IOException;

import dbsynchronizer.DBSynchronizer;
import pairtrading.PairTrading;

public class Application {
	
	
	public static void main(String[] args) throws IOException {

		
		String connString;
		if(args.length != 0) connString = args[0];
		else connString = "connections";
		
		PairTrading pt = new PairTrading(connString);		
		pt.Start();
		//*/
		
		/*
		DBSynchronizer dbSynchronizer = new DBSynchronizer();
		dbSynchronizer.Connect();
		//*/
	}
	

}
