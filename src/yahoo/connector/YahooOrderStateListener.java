package yahoo.connector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.OrderState;
import connector.OrderStateListener;
import connector.Trade;

public class YahooOrderStateListener  extends OrderStateListener {

	ObjectMapper mapper = new ObjectMapper();
	
	String URLString = "";
	String addOrderPathString = "";	
	String cancelOrderPathString = "";	
	String openedOrdersPathString = "";
	
	String closedOrdersPathString = "";
	
	int requestOrder = 0;
	
	HashMap<String, Order> activeOrdersMap;
	HashMap<String, Order> killedOrdersMap;
	
	ArrayList<Order> updatedOrders;
	ArrayList<Order> errorOrders;
	HashMap<Integer, Order> lostOrders;
	
	
	ArrayList<Order> newOrders;
	
	HashMap<String, Long> exchangeOrderIds;
	ArrayList<String> availableProducts;
	int availableProductsId = 0;
	
	public YahooOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		this.activeOrdersMap = new HashMap<String, Order>();
		this.killedOrdersMap = new HashMap<String, Order>();
		
		this.updatedOrders = new ArrayList<Order>();
		this.errorOrders = new ArrayList<Order>();
		this.newOrders = new ArrayList<Order>();
		this.lostOrders = new HashMap<Integer, Order>();
		
		
		this.exchangeOrderIds = new HashMap<String, Long>();
		
		this.availableProducts = new ArrayList<>();
	}

	
	
	
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		
		InitChildOrders();
		
		while(true){
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			ArrayList<Order> tmpArrayList_Order;
			synchronized(this.ordersToSend) {
				if(this.ordersToSend.size() > 0){
					tmpArrayList_Order = this.ordersToSend;
					this.ordersToSend = this.newOrders;
					this.newOrders = tmpArrayList_Order;
				}
			}
			
			for(Order order: this.newOrders) {
				switch(order.orderType) {
				case NewOrder:
					AddOrder(order);
				break;
				case KillOrder:
					Order activeOrder = this.activeOrdersMap.get(order.transIdList.get(0));
					if(activeOrder != null) KillOrder(activeOrder);
				break;
				default:
					break;
				}
			}			
			this.newOrders.clear();
			
			if(this.activeOrdersMap.size() > 0 || this.killedOrdersMap.size() > 0) {
				synchronized (this.floodControl) {
					int availableRequests = this.floodControl.AvailableRequests();
					
					if(availableRequests > 0){
						if(requestOrder == 0) {
							if(this.activeOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}else if(requestOrder == 1) {
							if(this.killedOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}


						switch(requestOrder) {
						case 0:
							CheckOpenedOrders();
							break;
						case 1:
							CheckClosedOrders();
							break;
						}
						
						
						requestOrder = (requestOrder + 1) % 2;					
						this.floodControl.NewRequest();
					}
				}
			}
			
			for(Order order : this.updatedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.updatedOrders.clear();
			

		}
	}
	
	private void InitChildOrders() {

	}
	
	private void CheckOpenedOrders() {
	
	}
	
	private void CheckClosedOrders() {
		

	}
	
	
	private void KillOrder(Order order) {
		
		
	}
	
	private void AddOrder(Order order) {
		//String jn = "Order approved.";

		System.out.println("ADD: " + order.toString() + ". Answer: Approved");
		double amount = order.amount;
		
		order.orderState = OrderState.Executed;
		order.amount = 0;
		this.updatedOrders.add(order);
		
		Long utcTimestamp = System.currentTimeMillis();
		
		Trade trade = new Trade();
		trade.setTrdPair(order.trdPair);
		trade.setAmount(amount);
		trade.setPrice(order.price);
		trade.setDir(order.dir.toString());
		trade.setMomentLinuxUTC(utcTimestamp);
		trade.setOrderAcceptanceId(utcTimestamp.toString());
		trade.setTradeId(trade.getOrderAcceptanceId());
		trade.setUserRefId(order.userref);
		trade.setPortfolioTradeId(order.portfolioTradeId);
		
		
		this.iOrderStateEvent.TradeExecuted(trade);
	}

	
}
