package yahoo.connector;

import java.util.ArrayList;
import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.MarketDataListener;


public class YahooMarketDataListener extends MarketDataListener {
	
	public YahooMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, connector.FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		

		HashMap<String, String> urlsMap = new HashMap<String, String>();
		//urlsMap.put("", "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22EURUSD%22%2C%20%22USDJPY%22%2C%20%22USDKRW%22%2C%20%22USDCNY%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=");
		urlsMap.put("", "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22EURKRW%22%2C%20%22EURCNY%22%2C%20%22EURJPY%22%2C%20%22EURUSD%22%2C%20%22USDJPY%22%2C%20%22USDKRW%22%2C%20%22USDCNY%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, YahooTicker.class)
			, "query:results:rate"
		);
		
		this.SetBrowserEmulation();
		
	}

}

