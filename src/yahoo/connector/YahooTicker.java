package yahoo.connector;

import interfaces.iTicker;

public class YahooTicker implements iTicker{
	
	private String id;
	private String Name;
	private double Rate;
	private String Date;
	private String Time;
	private double Ask;
	private double Bid;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getRate() {
		return Rate;
	}
	public void setRate(double rate) {
		Rate = rate;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	public double getAsk() {
		return Ask;
	}
	public void setAsk(double ask) {
		Ask = ask;
	}
	public double getBid() {
		return Bid;
	}
	public void setBid(double bid) {
		Bid = bid;
	}
	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return Bid;
	}
	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return Ask;
	}
	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return id;
	}
	
	
}
