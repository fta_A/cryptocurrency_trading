package yahoo.connector;


import connector.ExchangeConnector;

public class YahooConnector extends ExchangeConnector {
	public YahooConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
		
		this.connectionStateListener = new YahooConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new YahooMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);
		this.marketDataListenerThread.setName("yahoo");
		
		this.orderStateListener = new YahooOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);

	}

}
