package chbtc.connector;

import interfaces.iTicker;

public class ChbtcTicker implements iTicker{
	public double vol;
	public double last;
	public double sell;
	public double buy;
	public double high;
	public double low;
	
	private String trdPair;


	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return buy;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return sell;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
