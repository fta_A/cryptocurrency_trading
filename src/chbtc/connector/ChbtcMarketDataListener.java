package chbtc.connector;


import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class ChbtcMarketDataListener extends MarketDataListener {
	
	
	public ChbtcMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("btc_cny","http://api.chbtc.com/data/v1/ticker?currency=btc_cny");
		urlsMap.put("ltc_cny ","http://api.chbtc.com/data/v1/ticker?currency=ltc_cny ");
		urlsMap.put("eth_cny ","http://api.chbtc.com/data/v1/ticker?currency=eth_cny ");
		urlsMap.put("etc_cny ","http://api.chbtc.com/data/v1/ticker?currency=etc_cny ");
		urlsMap.put("bts_cny ","http://api.chbtc.com/data/v1/ticker?currency=bts_cny ");
		urlsMap.put("eos_cny ","http://api.chbtc.com/data/v1/ticker?currency=eos_cny ");
		urlsMap.put("bcc_cny ","http://api.chbtc.com/data/v1/ticker?currency=bcc_cny ");
		urlsMap.put("qtum_cny ","http://api.chbtc.com/data/v1/ticker?currency=qtum_cny ");
		urlsMap.put("hsr_cny ","http://api.chbtc.com/data/v1/ticker?currency=hsr_cny ");
	
		this.SetTickersListSource(
				urlsMap
				, mapper.getTypeFactory().constructType(ChbtcTicker.class)
				, "ticker"
			);
		
	}

	
	
	

}
