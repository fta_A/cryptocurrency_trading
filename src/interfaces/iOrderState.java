package interfaces;

public interface iOrderState {
	void SetOrderNum(String orderNum);
	String GetOrderNum();
	Long MomentLinuxUTC();
	
	String GetTrdPair();
	int GetUserRefId();
}
