package interfaces;

import connector.ConnectionState;

public interface iConnectionEvent {
	void ConnectionStateChanged(ConnectionState connectionState);
}
