package interfaces;

import connector.MarketData;

public interface iMarketDataEvent {
	long getUniqueID();
	void MarketDataChanged(int connectionId, MarketData marketData);
}
