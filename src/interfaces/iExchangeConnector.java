package interfaces;

import connector.ConnectionState;
import connector.Order;

public interface iExchangeConnector extends iConnectionEvent, iMarketDataEvent, iOrderStateEvent {
	String getConnectionString();	
	//void setConnectionString(String connectionString);

	String getExchangeCode();
	//void setExchangeCode(String exchangeCode);
	
	int getConnectionId();
	//void setConnectionId(int connectionId);
	
	void SetApiKey(String apiKey);
	void SetPrivateKey(String privateKey);
	
	
	void Connect();
	void Disconnect();	
	
	void AddConnectionEventListener(iConnectionEvent listener);
	void AddMarketDataListener(iMarketDataEvent listener);
	void AddOrderStateListener(iOrderStateEvent listener);
	
	void AddOrder(Order order);
	void KillOrder(Order order);
	
	void Subscribe(String tickerCode);
	void Subscribe(iMarketDataEvent listener, String tickerCode);
	void SubscribeAll();
	
	void GetBallance();
	void GetTrades(String trdPair, String from);
	
	ConnectionState.StateEnum GetConnectionState(ConnectionState.TypeEnum connectionStateType);
}
