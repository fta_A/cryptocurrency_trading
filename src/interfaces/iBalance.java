package interfaces;

public interface iBalance {
	void setExchange(String exchange);
	void setCurrencyCode(String currencyCode);
	void setBalanceValue(double balanceValue);
	
	String Exchange();
	String CurrencyCode();
	double BalanceValue();
	double Availlable();
	double Pending();
	String Address();
	int BalanceId();
}
