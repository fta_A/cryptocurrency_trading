package interfaces;

import java.util.ArrayList;

public interface iAddOrderResult {
	String getOrderNumber();
	ArrayList<iTrade> getOrderTrades();
}
