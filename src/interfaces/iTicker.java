package interfaces;

public interface iTicker {
	double Bid();
	double Ask();
	String Id();
	void setId(String id);
}
