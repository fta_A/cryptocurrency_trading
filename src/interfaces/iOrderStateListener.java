package interfaces;

import connector.Order;

public interface iOrderStateListener extends Runnable {
	void NewOrder(Order order);
	void SetApiKey(String apiKey);
	void SetPrivateKey(String privateKey);
	
	void RequestBallance();
	void RequestTrades(String trdPair, String from);
	
	void Disconnect();
	void ClearState();
}
