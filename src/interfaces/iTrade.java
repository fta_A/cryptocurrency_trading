package interfaces;

public interface iTrade {

	String getExchange();

	void setExchange(String exchange);

	String getTradeId();

	void setTradeId(String tradeId);

	String getOrderAcceptanceId();

	void setOrderAcceptanceId(String orderAcceptanceId);

	String getMomentStr();

	void setMomentStr(String momentStr);

	Long getMomentLinuxUTC();

	void setMomentLinuxUTC(Long momentLinuxUTC);

	String getTrdPair();

	void setTrdPair(String trdPair);

	String getDir();

	void setDir(String dir);

	double getAmount();

	void setAmount(double amount);

	double getPrice();

	void setPrice(double price);

	double getCommiss();

	void setCommiss(double commiss);

	int getUserRefId();

	void setUserRefId(int userRefId);
	
	long getPortfolioTradeId();
	
	void setPortfolioTradeId(long portfolioTradeId);

}