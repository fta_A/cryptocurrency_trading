package interfaces;

import connector.Order;

public interface iOrderStateEvent {
	void OrderStateChanged(Order orderState);
	void BalanceStateChanged(iBalance balance);
	void TradeExecuted(iTrade trade);
	
	void ExceptionIsAraised(String exceptionString);
}
