package interfaces;

import java.util.HashMap;

import com.fasterxml.jackson.databind.JavaType;

import connector.MarketDataListener.TickersListType;

public interface iMarketDataListener extends Runnable {
	void Subscribe(String tickerCode);
	void SubscribeAll();
	void SetTickersListSource(TickersListType listType, String separator, String urlForTickersList, JavaType jsonStructureForTickersList, String nestedChainForTickersList,  String urlForTicker, String urlForTickerEnd, JavaType jsonStructureForTicker, String nestedChainForTicker);
	void SetBrowserEmulation();
	void SetTickersListSource(HashMap<String, String> urlsMap,  JavaType jsonStructureForTicker, String nestedChainForTicker);
	
	void Disconnect();
	void ClearState();
}
