package interfaces;

public interface iConnectionStateListener extends Runnable {
	void MarketDataDisconnected();
	void MarketDataConnected();
	
	void OrderStateDisconnected();
	void OrderStateConnected();
	
	void Disconnect();
	void ClearState();
}
