package gdax.connector;

import interfaces.iTicker;

public class GdaxProduct  implements iTicker{
	private String id;
	private String base_currency;
	private String quote_currency;
	private double base_min_size;
	private double base_max_size;
	private double quote_increment;
	private String display_name;
	private String margin_enabled;
	private String status;
	private String status_message;
	
	
	public String getBase_currency() {
		return base_currency;
	}
	public void setBase_currency(String base_currency) {
		this.base_currency = base_currency;
	}
	public String getQuote_currency() {
		return quote_currency;
	}
	public void setQuote_currency(String quote_currency) {
		this.quote_currency = quote_currency;
	}
	public double getBase_min_size() {
		return base_min_size;
	}
	public void setBase_min_size(double base_min_size) {
		this.base_min_size = base_min_size;
	}
	public double getBase_max_size() {
		return base_max_size;
	}
	public void setBase_max_size(double base_max_size) {
		this.base_max_size = base_max_size;
	}
	public double getQuote_increment() {
		return quote_increment;
	}
	public void setQuote_increment(double quote_increment) {
		this.quote_increment = quote_increment;
	}
	public String getDisplay_name() {
		return display_name;
	}
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	public String getMargin_enabled() {
		return margin_enabled;
	}
	public void setMargin_enabled(String margin_enabled) {
		this.margin_enabled = margin_enabled;
	}
	public String getId() {
		return id;
	}
	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus_message() {
		return status_message;
	}
	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}
	
	
}
