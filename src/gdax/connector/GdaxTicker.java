package gdax.connector;

import java.util.ArrayList;

import interfaces.iTicker;

public class GdaxTicker implements iTicker{
	public Long sequence;
	public ArrayList<ArrayList<Double>> bids = null;
	public ArrayList<ArrayList<Double>> asks = null;

	private String trdPair;
	

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bids == null || bids.size() == 0 || bids.get(0).size() == 0 ? 0 : bids.get(0).get(0);
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return asks == null || asks.size() == 0 || asks.get(0).size() == 0 ? 0 : asks.get(0).get(0);
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
