package gdax.connector;

import connector.ExchangeConnector;

public class GdaxConnector extends ExchangeConnector {

	public GdaxConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new GdaxConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new GdaxMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

	}

}
