package gdax.connector;


import java.util.ArrayList;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class GdaxMarketDataListener extends MarketDataListener {
	
	
	public GdaxMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetTickersListSource(TickersListType.OneToOne
			, ""
			, "https://api.gdax.com/products"
			, mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, GdaxProduct.class)
			, ""
			, "https://api.gdax.com/products/"
			, "/book"
			, mapper.getTypeFactory().constructType (GdaxTicker.class)
			, ""
		);
		
		
		
	
	}

	
	
	

}
