package liqui.connector;

import interfaces.iTicker;

public class LiquiTicker implements iTicker{
	private String trdPair;

	public double high;
	public double low;
	public double avg;
	public double vol;
	public double vol_cur;
	public double last;
	public double buy;
	public double sell;
	public Long updated;
	
	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return buy;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return sell;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
