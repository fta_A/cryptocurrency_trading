package liqui.connector;

import java.util.Map;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class LiquiMarketDataListener extends MarketDataListener {
	
	
	public LiquiMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetTickersListSource(TickersListType.ManyToOne
				, "-"
				, "https://api.liqui.io/api/3/info"
				, mapper.getTypeFactory().constructMapType(Map.class, String.class, LiquiAssetPair.class)
				, "pairs"
				, "https://api.liqui.io/api/3/ticker/"
				, ""
				, mapper.getTypeFactory().constructMapType(Map.class, String.class, LiquiTicker.class)
				, ""
			);
		
		this.SetBrowserEmulation();
	
	}

	
	
	

}
