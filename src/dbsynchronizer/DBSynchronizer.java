package dbsynchronizer;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import connector.ConnectionState;
import connector.MarketData;
import interfaces.iConnectionEvent;
import interfaces.iExchangeConnector;
import interfaces.iMarketDataEvent;



public class DBSynchronizer implements iConnectionEvent, iMarketDataEvent { 
	HashMap<String, String> exchangesConnectionStrings;
	HashMap<Integer, iExchangeConnector> exchangeConnectors;
	
	/*
	String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	String DB_URL = "jdbc:mysql://151.80.45.53:32768/qa";
	String USER = "root";

	
	String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";	
	String DB_URL = "jdbc:sqlserver://151.80.45.53:1401;databaseName=qa";
	String USER = "SA";
	
	
	String PASS = "qFB4E97XpKHPxa";
	*/
	
	Connection conn = null;
	PreparedStatement preparedStmt = null;
	PreparedStatement truncateTmpTable = null;
	String dbInsertQuery;
	
	Long lastTmpTableClearing;
	
	Long lastTraceMsgMoment;
	HashSet<String> distinctExchanges = new HashSet<>();
	
	public DBSynchronizer() {
		// TODO Auto-generated constructor stub
	
		
		File f = new File("src/dbsynchronizer/connections");
		if(!f.exists())	f = new File("./connections");
		if(!f.exists()) {
			System.out.println("connections file is missed. exiting.");
			System.exit(0);
		}
		
		String jsonConnections = GetFileContents(f.getPath());
		
		if(jsonConnections == null) {
			System.out.println("connections file is missed. exiting.");
			System.exit(0);
		}			
		InitConnections(jsonConnections);
		
		
		f = new File("src/dbsynchronizer/dbstorage");
		if(!f.exists())	f = new File("./dbstorage");
		if(!f.exists()) {
			System.out.println("dbstorage file is missed. exiting.");
			System.exit(0);
		}
		
		String jsonDBStorage = GetFileContents(f.getPath());
		InitDBStorage(jsonDBStorage);
		
		lastTmpTableClearing = (long) 0;
		lastTraceMsgMoment = (long) 0;
	}
	
	private void InitDBStorage(String jsonStorage) {
		
		ObjectMapper mapper = new ObjectMapper();

		dbInsertQuery = "insert into ticker_data_tmp (ticker_timestamp, exchange_code, ticker_code, highest_bid, lowest_ask)"
				+ " values (?, ?, ?, ?, ?)";
		
		try {
			JsonNode jn = mapper.readTree(jsonStorage);
			DBConnector dbconn = mapper.convertValue(jn, DBConnector.class);
			
			Class.forName(dbconn.jdbc_driver);
			conn = DriverManager.getConnection(dbconn.db_url, dbconn.user, dbconn.pass);
			preparedStmt = conn.prepareStatement(dbInsertQuery);
			truncateTmpTable = conn.prepareStatement("truncate table ticker_data_tmp");

			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
	
	private String GetFileContents(String path) {
		
		try {
			Scanner in = new Scanner(new FileReader(path));
			StringBuilder sb = new StringBuilder();
			while(in.hasNext()) {
			    sb.append(in.next());
			}
			in.close();
			
			return sb.toString();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private void InitConnections(String jsonConnections) {
		ObjectMapper mapper = new ObjectMapper();
		exchangeConnectors = new HashMap<>();

		try {
			JsonNode jn = mapper.readTree(jsonConnections);
	        TypeReference<ArrayList<ExchConnection>> listTypeReference = new TypeReference<ArrayList<ExchConnection>> () {};
	        ArrayList<ExchConnection> list = mapper.convertValue(jn, listTypeReference);

	        for(ExchConnection conn : list) {
	        	try {
					Class<?> cl = Class.forName(conn.getClass_id());
					Constructor<?> con = cl.getConstructor(int.class, String.class);
					
					iExchangeConnector iExchangeConnector = (iExchangeConnector) con.newInstance(conn.getConnection_id(), conn.getConnection_string());
					
					iExchangeConnector.SetApiKey(conn.getApi_key());
					iExchangeConnector.SetPrivateKey(conn.getPrivate_key());
					
					iExchangeConnector.AddMarketDataListener(this);
					iExchangeConnector.AddConnectionEventListener(this);
					iExchangeConnector.SubscribeAll();
					
					exchangeConnectors.put(conn.getConnection_id(), iExchangeConnector);
					
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void Connect(){
		for (HashMap.Entry<Integer, iExchangeConnector> entry : exchangeConnectors.entrySet()) {
			entry.getValue().Connect();
		}

	}
	
	@Override
	public void ConnectionStateChanged(ConnectionState connectionState) {
		// TODO Auto-generated method stub
		
		System.out.println(exchangeConnectors.get(connectionState.connectionId).getExchangeCode() + ":" + connectionState.connectionType + " Connection state changed: " + connectionState.connectionState);
		
	}

	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub

		synchronized (this) {
			String momentStr = ZonedDateTime.now(ZoneOffset.UTC).toString();
			
			try {
				preparedStmt.setString (1, momentStr.substring(0, momentStr.length()-1));		
				preparedStmt.setString (2, exchangeConnectors.get(connectionId).getExchangeCode());
				preparedStmt.setString (3, marketData.getTrdPair());
				preparedStmt.setDouble (4, marketData.getHighestBid());
				preparedStmt.setDouble (5, marketData.getLowestAsk());
				
				//System.out.println(preparedStmt.toString());
				
				preparedStmt.execute();	
				
				/*
				if(System.currentTimeMillis() - lastTmpTableClearing - 60 * 1000 >= 0) {
					lastTmpTableClearing = System.currentTimeMillis();
					truncateTmpTable.execute();
				}
				*/
				
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				//System.out.println("Error in sql exec: " + preparedStmt.toString());
				System.out.println("Error in sql exec: " 
						+ "'" + momentStr.substring(0, momentStr.length()-1) + "',"
						+ "'" + exchangeConnectors.get(connectionId).getExchangeCode() + "',"
						+ "'" + marketData.getTrdPair() + "',"
						+ "'" + marketData.getHighestBid() + "',"
						+ "'" + marketData.getLowestAsk() + ""
						);
				
				
			}
			
			this.distinctExchanges.add(exchangeConnectors.get(connectionId).getExchangeCode());
			
			if(this.distinctExchanges.size() > 0 && System.currentTimeMillis() - lastTraceMsgMoment - 60 * 60 * 1000 >= 0) {
				String traceStr = "";
				for(String exch : this.distinctExchanges) {
					traceStr += ", " + exch;
				}
				
				System.out.println(distinctExchanges.size() + ": " + traceStr.substring(2));
				
				lastTraceMsgMoment = System.currentTimeMillis();
			}
			
		}
		
	}

	@Override
	public long getUniqueID() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}
