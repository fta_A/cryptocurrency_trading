package poloniex.connector;


import java.util.ArrayList;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.OrderDirection;
import connector.OrderStateListener;
import interfaces.iTrade;

public class PoloniexOrderStateListener  extends OrderStateListener {

	public PoloniexOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls("https://poloniex.com/tradingApi"
				, ""
				, ""
				, "POST"
				, "POST"
				);
		
		
		this.SetTickerAndBalanceTypes(
				// For Trades
				mapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, PoloniexTrade.class)
				, null
				
				// For Balance
				, mapper.getTypeFactory().constructMapType(Map.class, String.class, PoloniexBalance.class)
				, null
				
				// For Closed Orders
				, null
				, null
				
				// For Add order Results
				, mapper.constructType(PoloniexAddOrderResult.class)
				, null
				);

	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		String body = "nonce=" + nonce;
		
		switch(order.marketType) {
		case market:
			if(order.dir == OrderDirection.buy) {
				body += "&command=buy";
				order.price *= 1.5;
			}else {
				body += "&command=sell";
				order.price *= 0.9;
			}
			break;
		case limit:
			body += order.dir == OrderDirection.buy ? "&command=buy" : "&command=sell";
			break;
			
		}

		body += "&currencyPair=" + order.trdPair.toUpperCase();
		body += "&rate=" + decimalFormat.format(order.price);
		body += "&amount=" + decimalFormat.format(order.amount);
		
		
		

		return body;
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "nonce=" + nonce + "&command=returnCompleteBalances";

		return body;
	}

	@Override
	protected String GetTradesBody(String nonce, String trdPair, String from) {
		String body = "nonce=" + nonce 
				+ "&command=returnTradeHistory"
				+ "&currencyPair=" + trdPair.toUpperCase()
				+ "&start=" + from;

		return body;
	}
	
	@Override
	protected String GetFromForGetTradeRequest(iTrade trade) {
		return Long.toString(trade.getMomentLinuxUTC() / 1000 + 1);
	}

	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("Key", this.apiKey);
		httpsURLConnection.setRequestProperty("Sign", signature);		
	}
	
	@Override
	protected String GetStringToSign(String timestamp, String method, String path, String body) {
		return body;
	}

}
