package poloniex.connector;

import java.util.ArrayList;

import connector.AddOrderResult;
import interfaces.iTrade;

public class PoloniexAddOrderResult extends AddOrderResult{
	ArrayList<PoloniexTrade> resultingtrades;
	String error;
	
	public ArrayList<PoloniexTrade> getResultingtrades() {
		return resultingtrades;
	}
	public void setResultingtrades(ArrayList<PoloniexTrade> resultingtrades) {
		this.resultingtrades = resultingtrades;
		this.tradesList = new ArrayList<>();
		
		for(PoloniexTrade trade : resultingtrades) {
			iTrade iTr = trade;
			this.tradesList.add(iTr);
		}
		
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
		System.out.println(error);
	}
	
	
}
