package poloniex.connector;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import connector.Trade;

public class PoloniexTrade extends Trade{

	Long globaltradeid;
	String date;
	double rate;
	double total;
	double fee;
	Long ordernumber;
	String type;
	String category;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		date = date.toUpperCase();
		
		this.date = date;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date dt;
		
		try {
			dt = (Date) dateFormat.parse(this.date);
			this.momentLinuxUTC = dt.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Long getGlobaltradeid() {
		return globaltradeid;
	}

	public void setGlobaltradeid(Long globaltradeid) {
		this.globaltradeid = globaltradeid;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
		this.price = rate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
		this.commiss = fee;
	}

	public Long getOrdernumber() {
		return ordernumber;
	}

	public void setOrdernumber(Long ordernumber) {
		this.ordernumber = ordernumber;
		this.orderAcceptanceId = ordernumber.toString();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		this.dir = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
