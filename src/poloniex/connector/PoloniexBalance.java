package poloniex.connector;

import interfaces.iBalance;

public class PoloniexBalance implements iBalance{
	
	public double available;
	public double onOrders;
	public double btcValue;
	
	

	String currencyCode;
	String exchange;
	
	
	@Override
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	@Override
	public void setBalanceValue(double balanceValue) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String Exchange() {
		// TODO Auto-generated method stub
		return this.exchange;
	}

	@Override
	public String CurrencyCode() {
		// TODO Auto-generated method stub
		return this.currencyCode;
	}

	@Override
	public double BalanceValue() {
		// TODO Auto-generated method stub
		return this.available + this.onOrders;
	}

	@Override
	public double Availlable() {
		// TODO Auto-generated method stub
		return this.available;
	}

	@Override
	public double Pending() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String Address() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int BalanceId() {
		// TODO Auto-generated method stub
		return 0;
	}
	

	@Override
	public String toString() {
		return "Balance "
			+ "["
			+ "exchnage=" + this.Exchange()
			+ ", currency_code=" + this.CurrencyCode() 
			+ ", amount=" + this.BalanceValue()
			+ ", available=" + this.Availlable() 
			+ "]";
	}


}
