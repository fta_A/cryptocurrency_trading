package poloniex.connector;


import java.util.HashMap;
import java.util.Map;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class PoloniexMarketDataListener extends MarketDataListener {
	
	
	public PoloniexMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("", "https://poloniex.com/public?command=returnTicker");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructMapType(Map.class, String.class, PoloniexTicker.class)
			, ""
		);
	
	}

	
	
	

}
