package poloniex.connector;

import interfaces.iTicker;

public class PoloniexTicker implements iTicker{
	private String trdPair;
	
	private Integer id;
	private double last;
	private double lowestask;
	private double highestbid;
	private double percentchange;
	private double basevolume;
	private double quotevolume;
	private Integer isfrozen;
	private double high24hr;
	private double low24hr;

	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getLowestask() {
		return lowestask;
	}

	public void setLowestask(double lowestask) {
		this.lowestask = lowestask;
	}

	public double getHighestbid() {
		return highestbid;
	}

	public void setHighestbid(double highestbid) {
		this.highestbid = highestbid;
	}

	public double getPercentchange() {
		return percentchange;
	}

	public void setPercentchange(double percentchange) {
		this.percentchange = percentchange;
	}

	public double getBasevolume() {
		return basevolume;
	}

	public void setBasevolume(double basevolume) {
		this.basevolume = basevolume;
	}

	public double getQuotevolume() {
		return quotevolume;
	}

	public void setQuotevolume(double quotevolume) {
		this.quotevolume = quotevolume;
	}

	public Integer getIsfrozen() {
		return isfrozen;
	}

	public void setIsfrozen(Integer isfrozen) {
		this.isfrozen = isfrozen;
	}

	public double getHigh24hr() {
		return high24hr;
	}

	public void setHigh24hr(double high24hr) {
		this.high24hr = high24hr;
	}

	public double getLow24hr() {
		return low24hr;
	}

	public void setLow24hr(double low24hr) {
		this.low24hr = low24hr;
	}

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return highestbid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return lowestask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
