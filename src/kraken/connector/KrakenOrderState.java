package kraken.connector;

import connector.OrderState;

public class KrakenOrderState extends OrderState{

	public class OrderDescription{
		String pair;
		String type;
		String ordertype;
		double price;
		double price2;
		String leverage;
		String order;
		String close;
		public String getPair() {
			return pair;
		}
		public void setPair(String pair) {
			this.pair = pair;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getOrdertype() {
			return ordertype;
		}
		public void setOrdertype(String ordertype) {
			this.ordertype = ordertype;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public double getPrice2() {
			return price2;
		}
		public void setPrice2(double price2) {
			this.price2 = price2;
		}
		public String getLeverage() {
			return leverage;
		}
		public void setLeverage(String leverage) {
			this.leverage = leverage;
		}
		public String getOrder() {
			return order;
		}
		public void setOrder(String order) {
			this.order = order;
		}
		public String getClose() {
			return close;
		}
		public void setClose(String close) {
			this.close = close;
		}
		
		
	}

	
	
	String refid;
	int userref;
	String status;
	String reason;
	Long opentm;
	Long starttm;
	Long expiretm;
	Long closetm;
	OrderDescription descr;
	double vol;
	double vol_exec;
	double cost;
	double fee;
	double price;
	double stopprice;
	double limitprice;
	String misc;
	String oflags;
	Object trades;
	
	
	public String getRefid() {
		return refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public int getUserref() {
		return userref;
	}

	public void setUserref(int userref) {
		this.userref = userref;
		this.userReferenceId = userref;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getOpentm() {
		return opentm;
	}

	public void setOpentm(Long opentm) {
		this.opentm = opentm;
		
		//this.momentLinuxUTC = (long) opentm * 1000;
	}

	public Long getStarttm() {
		return starttm;
	}

	public void setStarttm(Long starttm) {
		this.starttm = starttm;
	}

	public Long getExpiretm() {
		return expiretm;
	}

	public void setExpiretm(Long expiretm) {
		this.expiretm = expiretm;
	}

	public OrderDescription getDescr() {
		return descr;
	}

	public void setDescr(OrderDescription descr) {
		this.descr = descr;
		this.tradingPair = descr.pair;
	}

	public double getVol() {
		return vol;
	}

	public void setVol(double vol) {
		this.vol = vol;
	}

	public double getVol_exec() {
		return vol_exec;
	}

	public void setVol_exec(double vol_exec) {
		this.vol_exec = vol_exec;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getStopprice() {
		return stopprice;
	}

	public void setStopprice(double stopprice) {
		this.stopprice = stopprice;
	}

	public double getLimitprice() {
		return limitprice;
	}

	public void setLimitprice(double limitprice) {
		this.limitprice = limitprice;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	public String getOflags() {
		return oflags;
	}

	public void setOflags(String oflags) {
		this.oflags = oflags;
	}

	public Object getTrades() {
		return trades;
	}

	public void setTrades(Object trades) {
		this.trades = trades;
	}

	public Long getClosetm() {
		return closetm;
	}

	public void setClosetm(Long closetm) {
		this.closetm = closetm;
		
		this.momentLinuxUTC = (long) closetm * 1000;
	}

	
}
