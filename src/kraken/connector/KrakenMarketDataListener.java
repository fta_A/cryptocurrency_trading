package kraken.connector;


import java.util.Map;

import connector.ConnectionStateListener;
import connector.MarketDataListener;


public class KrakenMarketDataListener extends MarketDataListener {
	
	
	public KrakenMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, connector.FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		this.SetTickersListSource(TickersListType.ManyToOne
				, ","
				, "https://api.kraken.com/0/public/AssetPairs"
				, mapper.getTypeFactory().constructMapType(Map.class, String.class, KrakenAssetPair.class)
				, "result"
				, "https://api.kraken.com/0/public/Ticker?pair="
				, ""
				, mapper.getTypeFactory().constructMapType(Map.class, String.class, KrakenTicker.class)
				, "result"
			);

		
	}


}

