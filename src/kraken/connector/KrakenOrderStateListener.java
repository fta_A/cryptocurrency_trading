package kraken.connector;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.Order.MarketType;
import connector.OrderStateListener;
import interfaces.iOrderState;
import interfaces.iTrade;

public class KrakenOrderStateListener   extends OrderStateListener {

	public KrakenOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		this.SetUrls("https://api.kraken.com"
				, "/0/private/AddOrder"
				, "/0/private/CancelOrder"
				, "POST /0/private/Balance"
				, "POST /0/private/TradesHistory"
				, "POST /0/private/ClosedOrders"
				);
		
		
		this.SetTickerAndBalanceTypes(
				mapper.getTypeFactory().constructMapLikeType(Map.class, String.class, KrakenTrade.class)
				, "result:trades"
				, mapper.getTypeFactory().constructMapLikeType(Map.class, String.class, Double.class)
				, "result"
				, mapper.getTypeFactory().constructMapLikeType(Map.class, String.class, KrakenOrderState.class)
				, "result:closed"
				, mapper.getTypeFactory().constructType(KrakenAddOrderResult.class)
				, "result"
				);

		
	}

	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		String postData = "nonce=" + nonce;
		postData += "&pair=" + order.trdPair;
		postData += "&type=" + order.dir;
		postData += "&ordertype=" + order.marketType;
		postData += "&price=" + decimalFormat.format(order.price);
		postData += "&volume=" + decimalFormat.format(order.amount);
		postData += "&userref=" + order.userref;
		
		if(order.marketType == MarketType.market)
			postData += "&trading_agreement=agree";

		
		return postData;
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "nonce=" + nonce;

		return body;
	}
	
	@Override
	protected String GetTradesBody(String nonce, String trdPair, String from) {
		String body = "nonce=" + nonce 
				+ "&start=" + from;

		return body;
	}
	
	@Override
	protected String GetClosedOrdersBody(String nonce, String trdPair, String from) {
		String body = "nonce=" + nonce 
				+ "&start=" + from;

		return body;
	}
	
	@Override
	protected String GetFromForGetTradeRequest(iTrade trade) {
		return Long.toString(trade.getMomentLinuxUTC() / 1000 + 1);
	}
	
	@Override
	protected String GetFromForClosedOrdersRequest(iOrderState orderState) {
		return Long.toString(orderState.MomentLinuxUTC() / 1000 + 1);
	}

	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		httpsURLConnection.setRequestProperty("API-Key", this.apiKey);
		httpsURLConnection.setRequestProperty("API-Sign", signature);
	}
	
	@Override
	protected String calculateSignature(String nonce, String method, String path, String body) {
	    String signature = "";
	    try {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update((nonce + body).getBytes());
	        
	        Mac mac = Mac.getInstance("HmacSHA512");
	        mac.init(new SecretKeySpec(Base64.getDecoder().decode(this.privateKey.getBytes()), "HmacSHA512"));
	        mac.update(path.getBytes());
	        signature = new String(Base64.getEncoder().encode(mac.doFinal(md.digest())));
	    } catch(Exception e) {}
	    return signature;		
	}
}
