package kraken.connector;

import interfaces.iBalance;

public class KrakenBalance implements iBalance{

	double balanceValue;
	String currencyCode;
	String exchange;

	
	@Override
	public void setExchange(String exchange) {
		// TODO Auto-generated method stub
		this.exchange = exchange;
	}

	@Override
	public void setCurrencyCode(String currencyCode) {
		// TODO Auto-generated method stub
		this.currencyCode = currencyCode;
	}

	@Override
	public void setBalanceValue(double balanceValue) {
		// TODO Auto-generated method stub
		this.balanceValue = balanceValue;
	}

	
	@Override
	public String Exchange() {
		// TODO Auto-generated method stub
		return this.exchange;
	}

	@Override
	public String CurrencyCode() {
		// TODO Auto-generated method stub
		return this.currencyCode;
	}

	@Override
	public double BalanceValue() {
		// TODO Auto-generated method stub
		return this.balanceValue;
	}

	@Override
	public double Availlable() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double Pending() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String Address() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int BalanceId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString() {
		return "Balance "
				+ "["
				+ "exchnage=" + this.Exchange()
				+ ", currency_code=" + this.CurrencyCode() 
				+ ", amount=" + this.BalanceValue()
				+ ", available=" + this.Availlable() 
				+ "]";
	}



}
