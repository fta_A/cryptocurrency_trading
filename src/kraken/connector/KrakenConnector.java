package kraken.connector;

import connector.ExchangeConnector;

public class KrakenConnector extends ExchangeConnector{
	
	public KrakenConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub

		this.connectionStateListener = new KrakenConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new KrakenMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);
	
		this.orderStateListener = new KrakenOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);	
	}
	
	
	
}
