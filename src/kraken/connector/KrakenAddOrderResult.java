package kraken.connector;

import java.util.ArrayList;

import connector.AddOrderResult;

public class KrakenAddOrderResult extends AddOrderResult {
	private KrakenAddOrderDescription descr;
	private ArrayList<String> txid;
	
	public KrakenAddOrderDescription getDescr() {
		return descr;
	}
	public void setDescr(KrakenAddOrderDescription descr) {
		this.descr = descr;
	}
	public ArrayList<String> getTxid() {
		return txid;
	}
	public void setTxid(ArrayList<String> txid) {
		this.txid = txid;		
		this.orderNumber = txid != null && txid.size() > 0 ? txid.get(0) : null;
	}
	
	
}
