package kraken.connector;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import connector.Trade;


public class KrakenTrade extends Trade{

	String ordertxid;
	String pair;
	Long time;
	String type;
	String ordertype;
	double cost;
	double fee;
	double vol;
	double margin;
	String misc;
	
	public Long getTime() {
		return time;
	}
	
	public void setTime(Long time) {
		this.time = time;
		this.momentLinuxUTC = (long) time * 1000;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		this.momentStr = dateFormat.format(new Date(momentLinuxUTC));
	}

	public String getOrdertxid() {
		return ordertxid;
	}

	public void setOrdertxid(String ordertxid) {
		this.ordertxid = ordertxid;
		this.orderAcceptanceId = ordertxid;
	}

	public String getPair() {
		return pair;
	}

	public void setPair(String pair) {
		this.pair = pair;
		this.trdPair = pair;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		this.dir = type;
	}

	public String getOrdertype() {
		return ordertype;
	}

	public void setOrdertype(String ordertype) {
		this.ordertype = ordertype;
	}


	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
		
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
		this.commiss = fee;
	}

	public double getVol() {
		return vol;
	}

	public void setVol(double vol) {
		this.vol = vol;
		this.amount = vol;
	}

	public double getMargin() {
		return margin;
	}

	public void setMargin(double margin) {
		this.margin = margin;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}
	
	
	
}
