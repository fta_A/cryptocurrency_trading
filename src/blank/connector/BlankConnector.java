package blank.connector;

import connector.ExchangeConnector;

public class BlankConnector extends ExchangeConnector {

	public BlankConnector(int connectionId, String connectionString) {
		super(connectionId, connectionString);
		// TODO Auto-generated constructor stub
	
		this.connectionStateListener = new BlankConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new BlankMarketDataListener(this, this.floodControl, this.connectionStateListener);
		this.marketDataListenerThread = new Thread(this.marketDataListener);

		this.orderStateListener = new BlankOrderStateListener(this, this.floodControl, this.connectionStateListener);
		this.orderStateListenerThread = new Thread(this.orderStateListener);
	}

}
