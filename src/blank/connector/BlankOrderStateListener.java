package blank.connector;


import javax.net.ssl.HttpsURLConnection;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.Order;
import connector.OrderStateListener;

public class BlankOrderStateListener  extends OrderStateListener {

	public BlankOrderStateListener(interfaces.iOrderStateEvent iOrderStateEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iOrderStateEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		this.SetUrls(""
				, ""
				, ""
				, ""
				, ""
				);
		
		this.SetSignMethod("HmacSHA256");
	}
	
	
	@Override
	protected String GetAddOrderBody(Order order, String nonce) {
		String body = "";

		return body;
	}
	
	@Override
	protected String GetBalanceBody(String nonce) {
		String body = "";

		return body;
	}

	
	@Override
	protected void SetRequestProperties(HttpsURLConnection httpsURLConnection, String signature, String timestamp, String body) {
		
	}
	
	@Override
	protected String GetStringToSign(String timestamp, String method, String path, String body) {
		return body;
	}
	
	@Override
	protected String AdjustBodyAfterBodySign(String path, String body, String signature) {
		return body;
	}

	@Override
	protected String AdjustPathAfterBodySign(String path, String body, String signature) {
		return path;
	}
	
	
}
