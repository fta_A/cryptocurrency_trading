package coinone.connector;


import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class CoinoneMarketDataListener extends MarketDataListener {
	
	
	public CoinoneMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("bchkrw", "https://api.coinone.co.kr/orderbook/?currency=bch&format=json");
		urlsMap.put("btckrw", "https://api.coinone.co.kr/orderbook/?currency=btc&format=json");
		urlsMap.put("ethkrw", "https://api.coinone.co.kr/orderbook/?currency=eth&format=json");
		urlsMap.put("etckrw", "https://api.coinone.co.kr/orderbook/?currency=etc&format=json");
		urlsMap.put("xrpkrw", "https://api.coinone.co.kr/orderbook/?currency=xrp&format=json");
		urlsMap.put("qtumkrw", "https://api.coinone.co.kr/orderbook/?currency=qtum&format=json");
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructType(CoinoneTicker.class)
			, ""
		);

		
	}

	
	
	

}
