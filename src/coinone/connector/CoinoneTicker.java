package coinone.connector;

import java.util.ArrayList;
import java.util.Map;

import interfaces.iTicker;

public class CoinoneTicker implements iTicker{
	private String trdPair;
	
	private Long timestamp;
	public ArrayList<Map<String, Double>> bid = new ArrayList<>();
	public ArrayList<Map<String, Double>> ask = new ArrayList<>();
	private Long errorcode;
	private String currency;
	private String result;

	

	public String getTrdPair() {
		return trdPair;
	}

	public void setTrdPair(String trdPair) {
		this.trdPair = trdPair;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}



	public ArrayList<Map<String, Double>> getBid() {
		return bid;
	}

	public void setBid(ArrayList<Map<String, Double>> bid) {
		this.bid = bid;
	}

	public ArrayList<Map<String, Double>> getAsk() {
		return ask;
	}

	public void setAsk(ArrayList<Map<String, Double>> ask) {
		this.ask = ask;
	}

	public Long getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(Long errorcode) {
		this.errorcode = errorcode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public class PriceQty{
		public double qty;
		public double price;
	}

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid == null || bid.size() == 0 ? 0 : bid.get(0).get("price");
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask == null || ask.size() == 0 ? 0 : ask.get(0).get("price");
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
