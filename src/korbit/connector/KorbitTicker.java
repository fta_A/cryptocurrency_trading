package korbit.connector;

import interfaces.iTicker;

public class KorbitTicker implements iTicker{
	private String trdPair;

	public Long timestamp;
	public Double last;
	public Double bid;
	public Double ask;
	public Double low;
	public Double high;
	public Double volume;
	public Double change;
	public Double changepercent;

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
