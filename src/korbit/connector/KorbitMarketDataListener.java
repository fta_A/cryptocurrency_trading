package korbit.connector;


import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class KorbitMarketDataListener extends MarketDataListener {
	
	
	public KorbitMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("btc_krw", "https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=btc_krw");
		urlsMap.put("etc_krw", "https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=etc_krw");
		urlsMap.put("eth_krw", "https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=eth_krw");
		urlsMap.put("xrp_krw", "https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=xrp_krw");
		
		this.SetTickersListSource(
				urlsMap
				, mapper.getTypeFactory().constructType(KorbitTicker.class)
				, ""
			);
	
		this.SetBrowserEmulation();
	}

	
	
	

}
