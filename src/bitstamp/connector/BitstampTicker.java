package bitstamp.connector;

import interfaces.iTicker;

public class BitstampTicker implements iTicker{

	private String trdPair;
	
	public double high;
	public double last;
	public double timestamp;
	public double bid;
	public double vwap;
	public double volume;
	public double low;
	public double ask;
	public double open;

	@Override
	public double Bid() {
		// TODO Auto-generated method stub
		return bid;
	}

	@Override
	public double Ask() {
		// TODO Auto-generated method stub
		return ask;
	}

	@Override
	public String Id() {
		// TODO Auto-generated method stub
		return trdPair;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		trdPair = id;
	}

}
