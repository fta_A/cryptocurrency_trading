package bitstamp.connector;



import java.util.HashMap;

import connector.ConnectionStateListener;
import connector.FloodControl;
import connector.MarketDataListener;

public class BitstampMarketDataListener extends MarketDataListener {
	
	
	public BitstampMarketDataListener(interfaces.iMarketDataEvent iMarketDataEvent, FloodControl floodControl,
			ConnectionStateListener connectionStateListener) {
		super(iMarketDataEvent, floodControl, connectionStateListener);
		// TODO Auto-generated constructor stub
		
		HashMap<String, String> urlsMap = new HashMap<String, String>();
		urlsMap.put("btcusd","https://www.bitstamp.net/api/v2/ticker/btcusd/");
		urlsMap.put("btceur","https://www.bitstamp.net/api/v2/ticker/btceur/");
		urlsMap.put("eurusd","https://www.bitstamp.net/api/v2/ticker/eurusd/");
		urlsMap.put("xrpusd","https://www.bitstamp.net/api/v2/ticker/xrpusd/");
		urlsMap.put("xrpeur","https://www.bitstamp.net/api/v2/ticker/xrpeur/");
		urlsMap.put("xrpbtc","https://www.bitstamp.net/api/v2/ticker/xrpbtc/");
		urlsMap.put("ltcusd","https://www.bitstamp.net/api/v2/ticker/ltcusd/");
		urlsMap.put("ltceur","https://www.bitstamp.net/api/v2/ticker/ltceur/");
		urlsMap.put("ltcbtc","https://www.bitstamp.net/api/v2/ticker/ltcbtc/");
		urlsMap.put("ethusd","https://www.bitstamp.net/api/v2/ticker/ethusd/");
		urlsMap.put("etheur","https://www.bitstamp.net/api/v2/ticker/etheur/");
		urlsMap.put("ethbtc","https://www.bitstamp.net/api/v2/ticker/ethbtc/");
		
		
		this.SetTickersListSource(
			urlsMap
			, mapper.getTypeFactory().constructType(BitstampTicker.class)
			, ""
		);
		

	
	}

	
	
	

}
